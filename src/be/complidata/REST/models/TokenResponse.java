package be.complidata.REST.models;

import java.io.Serializable;

public class TokenResponse implements Serializable {
	private static final long serialVersionUID = 7186833265657776278L;
	
	private String access_token, token_type;
	private int expires_in_minutes;
	
	
	public TokenResponse() { }
	
	public TokenResponse(String accessToken, String tokenType, int expiryMinutes) {
		this.access_token 		= accessToken;
		this.token_type 		= tokenType;
		this.expires_in_minutes = expiryMinutes;
	}
	
	public String getAccess_token() {
		return access_token;
	}
	public int getExpires_in_minutes() {
		return expires_in_minutes;
	}
	public String getToken_type() {
		return token_type;
	}
	public void setAccess_token(String access_token) {
		this.access_token = access_token;
	}
	public void setExpires_in_minutes(int expires_in_minutes) {
		this.expires_in_minutes = expires_in_minutes;
	}
	public void setToken_type(String token_type) {
		this.token_type = token_type;
	}
}
