package be.complidata.REST;

import java.util.Optional;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ws.rs.HttpMethod;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.client.InvocationCallback;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.logging.LoggingFeature;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;


public class ClientJersey {
	private Client client;
	private Optional<String> authToken;

	private final Logger LOG = Logger.getLogger("ClientJersey"); 
	
	
	public ClientJersey() {
		this(new Class<?>[] {});
	}

	public ClientJersey(Class<?>[] classesToRegister) {
		authToken = Optional.empty();
		
		// Client set-up with support for requested feature classes
		ClientBuilder clientBuilder = ClientBuilder.newBuilder();
		for (Class<?> clazz : classesToRegister)
			clientBuilder.register(clazz);
		client = clientBuilder.build();

		client.register(new LoggingFeature(LOG, Level.INFO, null, null));
	}
	
	
	
	// TODO
	/*
        JsonObject jsonCredentials = new JsonObject();
        jsonCredentials.addProperty("clientID", "9e887aa2746144bf9c80412f44ab2df2");
        jsonCredentials.addProperty("clientSecret", "jXbg0wbr7g0UdeHUHW0h7nNmFGxpcE3QDBXLRFuRzbQ");
    */
	public void authenticate(String url, JsonObject payload) {
        WebTarget webTarget = client.target(url);
        
        // Make a invocationBuilder to make the request and set the credentials in the header.
        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
        invocationBuilder.header("x-dnb-user", payload.get("x-dnb-user").getAsString());
        invocationBuilder.header("x-dnb-pwd", payload.get("x-dnb-pwd").getAsString());
        
        // Do a POST with null, as the credentials are in the Header already.
        Response response = invocationBuilder.post(Entity.json(null));
        System.out.println("response = <" + response + ">");
        String jsonResponse = response.readEntity(String.class);
        
        if (response.getStatus() != 200) {
            System.out.println("An error occurred :  " + jsonResponse);
        } 
        else {
        	// Parse the string to a JsonObject.
            JsonElement jsonResponseParsed = new JsonParser().parse(jsonResponse);
    	    JsonObject jsonRoot = jsonResponseParsed.getAsJsonObject();
    	    
    	    // Check if the response contains valid AuthenticationDetail > Token, if so -> set it in AuthThoken object.
            if(jsonRoot.has("AuthenticationDetail")) {
        		JsonObject jsonToken = jsonRoot.getAsJsonObject("AuthenticationDetail");
        		
        		if(!jsonToken.get("Token").getAsString().isEmpty()) {
        			System.out.println("TOKEN = <" + jsonToken.get("Token").getAsString() + ">");
        			authToken = Optional.of(jsonToken.get("Token").getAsString());
        		}
        		else {
        			System.out.println("TOKEN WAS EMPTY...");
        		}
            }
            else {
            	System.out.println("NO AUTHENTICATIONDETAIL WAS PRESENT...");
            }
            
            System.out.println("Authentication token received:  " + authToken.get());
        }
	}
	
	
	public Response request(String url, Entity<?> entity, String method, boolean async) {
        WebTarget webTarget = client.target(url);
    	Builder responseBuilder = webTarget.request(MediaType.APPLICATION_JSON).acceptEncoding("utf-8","utf-16");
    	
    	if (authToken.isPresent()) 
    		responseBuilder.header(HttpHeaders.AUTHORIZATION, authToken.get());
    	
    	return async ? async(responseBuilder,entity,method) : sync(responseBuilder,entity,method);
	}
	
	
	private Response sync(Builder responseBuilder, Entity<?> entity, String method) {
    	return responseBuilder.method(method,entity);
	}
	
	
	private Response async(Builder responseBuilder, Entity<?> entity, String method) {
    	Future<Response> futureResponse = responseBuilder.async().method(method,entity,new InvocationCallback<Response>() {
            @Override
            public void completed(Response response) {
                System.out.println("Response entity '" + response + "' received.");
            }
 
            @Override
            public void failed(Throwable throwable) {
                System.out.println("Invocation failed.");
                throwable.printStackTrace();
            }
        });
    	
    	try {
    		Response response = futureResponse.get();
			return response;
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
    	
    	return null;
	}
}
