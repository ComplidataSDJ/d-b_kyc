package be.complidata.REST;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.ws.rs.HttpMethod;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;

import be.complidata.models.BeneficialOwner;
import be.complidata.models.Company;
import be.complidata.models.DetailedCompany;

public class ClientDnB {
	private ClientJersey clientREST;
	
	
	public ClientDnB() {
		this.clientREST = new ClientJersey();
	}

	
	/**
	 * Method to authenticate to the D&B API
	 */
	public void authenticate() {
		String url = "https://direct.dnb.com/Authentication/V2.0/";
		
		// Build JsonObject with header credentials
		JsonObject jsonCredentials = new JsonObject();
		jsonCredentials.addProperty("x-dnb-user", "P100000DFA8518AF3174653A16FFFC97");
		jsonCredentials.addProperty("x-dnb-pwd", "@S0mbeke13");
		
		clientREST.authenticate(url, jsonCredentials);
	}
	
	
	/**
	 * Lookup with subject name to retrieve list of matched companies for your input.
	 * 
	 * @param companyName				String representing the company name
	 * @param ISOCode					String representing the CountryISOCode (2)
	 * 
	 * @return							List<Company> representing a list with all the matched companies for the name & ISO code you gave
	 */
	public List<Company> lookupCompanyMatchesBySubjectName(String companyName, String ISOCode) {
		// Build the url with the parameters.
		String url = "https://direct.dnb.com/V6.0/organizations?"
				+ "match=true&"
				+ "SubjectName=" + companyName
				+ "&CountryISOAlpha2Code=" + ISOCode;
		
		// Use the Request method from the ClientJersey Object.
		Response response = clientREST.request(url, null, HttpMethod.GET, false);
        String jsonResponse = response.readEntity(String.class);
        
        List<Company> matchedCompanies = new ArrayList<Company>();
        // If the response is not OK (= 200), error, else parse the response to a List<Company>.
        // TODO Exception?
        if (response.getStatus() != 200) {
            System.out.println("An error occurred :  " + jsonResponse);
        } 
        else {
        	matchedCompanies = parseMatchedCompanyResult(jsonResponse);
        }
        
        return matchedCompanies;
	}
	
	
	/**
	 * Lookup with company registration code to request a 'simple' Company Object for your input.
	 * 
	 * @param companyRegistrationCode	String representing the companyRegistrationCode
	 * @param ISOCode					String representing the CountryISOCode (2)
	 * 
	 * @return							Company representing the company for the RegistrationCode that you entered
	 */
	public Company lookupCompanyMatchesByRegCode(String companyRegistrationCode, String ISOCode) {
		// Build the url with the parameters.
		String url = "https://direct.dnb.com/V6.0/organizations?"
				+ "match=true&"
				+ "OrganizationIdentificationNumber=" + companyRegistrationCode
				+ "&CountryISOAlpha2Code=" + ISOCode;
		
		// Use the Request method from the ClientJersey Object.
		Response response = clientREST.request(url, null, HttpMethod.GET, false);
        String jsonResponse = response.readEntity(String.class);
        
        List<Company> matchedCompanies  = new ArrayList<Company>();
        Company matchedCompany 			= null;
        // If the response is not OK (= 200), error, else parse the response to a List<Company>.
        // TODO Exception?
        if (response.getStatus() != 200) {
            System.out.println("An error occurred :  " + jsonResponse);
        } 
        else {
        	matchedCompanies = parseMatchedCompanyResult(jsonResponse);
        	
        	if(matchedCompanies.size() == 1) {
        		matchedCompany = matchedCompanies.get(0);
        	}
        }
        System.out.println(matchedCompany);
        return matchedCompany;
	}
	
	
	/**
	 * Method to lookup a DetailedCompany with the DUNS number of that company
	 * 
	 * @param numberDUNS	String representing the DUNS-number of the company you are trying to request
	 * 
	 * @return				DetailedCompnay object with the resulted company parsed into a DetailedCompany
	 */
	public DetailedCompany lookupDetailedCompanyByDUNS(String numberDUNS) {
		String url = "https://direct.dnb.com/V7.0/organizations/" + numberDUNS +"/products/DCP_PREM";
		
		// Use the Request method from the ClientJersey Object.
		Response response = clientREST.request(url, null, HttpMethod.GET, false);
        String jsonResponse = response.readEntity(String.class);
        
        DetailedCompany resultDetailedCompany = null;
        // If the response is not OK (= 200), error, else parse the response to a List<Company>.
        // TODO Exception?
        if (response.getStatus() != 200) {
            System.out.println("An error occurred :  " + jsonResponse);
        } 
        else {
        	resultDetailedCompany = parseDetailedCompanyResult(jsonResponse);
        }
        
        return resultDetailedCompany;
	}
	
	
	/**
	 * Method to lookup the Ultimate Beneficial Owners of a Company by its DUNS-number 
	 * 
	 * @param numberDUNS	String representing the DUNS number of the Company
	 * 
	 * @return				List<BeneficialOwner> representing a list with the UBO's of the company
	 */
	public List<BeneficialOwner> lookupUBOByDUNS(String numberDUNS) {
		String url = "https://direct.dnb.com/V6.0/organizations/" + numberDUNS +"/products/CMP_BOL";
		
		// Use the Request method from the ClientJersey Object.
		Response response = clientREST.request(url, null, HttpMethod.GET, false);
        String jsonResponse = response.readEntity(String.class);
        
        List<BeneficialOwner> resultUBOS = null;
        // If the response is not OK (= 200), error, else parse the response to a List<Company>.
        // TODO Exception?
        if (response.getStatus() != 200) {
            System.out.println("An error occurred :  " + jsonResponse);
        } 
        else {
        	resultUBOS = parseUBOResult(jsonResponse);
        }
        
        //DEBUG
//        System.out.println(resultUBOS.size());
//        for(BeneficialOwner b : resultUBOS) {
//            System.out.println("UBO = " + b.getName());
//        }
        
        return resultUBOS;
	}
	
	
	//PARSERS
	//=======
	/**
	 * Parse the matched company response with 'simple' Company objects.
	 * 
	 * @param jsonResponseString	String representing the response string
	 * 
	 * @return						List<Company> representing a list with all the matched companies for your input.
	 */
	private List<Company> parseMatchedCompanyResult(String jsonResponseString) {

		// Debug
//		System.out.println("Json Response String   <" + jsonResponseString + ">");
		
		List<Company> result = new ArrayList<>();
		
		// For all matched candidate companies in the specified JSON response string
		JsonObject jsonRoot = new JsonParser().parse(jsonResponseString).getAsJsonObject();
		JsonArray matchCandidates = jsonRoot.getAsJsonObject("MatchResponse").getAsJsonObject("MatchResponseDetail").getAsJsonArray("MatchCandidate");
		for (int i = 0; i < matchCandidates.size(); i++) {
			
			// JSON root of current matched candidate
			JsonObject currentCandidate = matchCandidates.get(i).getAsJsonObject();

			// DUNS number
			JsonPrimitive jsonDUNS = currentCandidate.getAsJsonPrimitive("DUNSNumber");
			String numberDUNS 	   = (jsonDUNS == null) ? "unknown" : jsonDUNS.getAsString();
			
			// Company name
			JsonObject jsonOrgPrimName = currentCandidate.getAsJsonObject("OrganizationPrimaryName");
			JsonObject jsonOrgName 	   = (jsonOrgPrimName == null) ? null      : jsonOrgPrimName.getAsJsonObject("OrganizationName");
			String companyName 		   = (jsonOrgName == null) 	   ? "unknown" : jsonOrgName.getAsJsonPrimitive("$").getAsString();

			// Phone number
//			JsonObject jsonRootPhone = currentCandidate.getAsJsonObject("TelephoneNumber");
//			String phoneNumber = jsonRootPhone.getAsJsonPrimitive("TelecommunicationNumber").getAsString();
			
			
			// Address
			JsonObject jsonRootAddress = currentCandidate.getAsJsonObject("PrimaryAddress");
			if (jsonRootAddress == null)
				continue;
			
			StringBuilder sbAddress = new StringBuilder();
			JsonArray jsonArrayAddressParts = jsonRootAddress.getAsJsonArray("StreetAddressLine");
			
			if (jsonArrayAddressParts == null)
				sbAddress.append("unknown");
			else {
				for (int j = 0; j < jsonArrayAddressParts.size(); j++) {
					JsonPrimitive jsonAddress = jsonArrayAddressParts.get(j).getAsJsonObject().getAsJsonPrimitive("LineText");
					sbAddress.append((jsonAddress == null) ? "unknown" : jsonAddress.getAsString());
				}
			}
			String address = sbAddress.toString();
			
			// Postal code
			JsonPrimitive jsonPostal = jsonRootAddress.getAsJsonPrimitive("PostalCode");
			String postalCode 	  	 = (jsonPostal == null) ? "uknown" : jsonPostal.getAsString();
			
			// City / town
			JsonPrimitive jsonPrimTown = jsonRootAddress.getAsJsonPrimitive("PrimaryTownName");
			String cityTown 		   = (jsonPrimTown == null) ? "uknown" : jsonPrimTown.getAsString();
			
			// Country ISO code
			JsonPrimitive jsonCountry = jsonRootAddress.getAsJsonPrimitive("CountryISOAlpha2Code");
			String countryISO 		  = (jsonCountry == null) ? "uknown" : jsonCountry.getAsString();
			
			
			// Add candidate company to result builder
			// TODO, add registration code to company -> more information, better!
			result.add(new Company(companyName, numberDUNS, address, postalCode, cityTown, countryISO));
		}
		
		return result;
	}
	
	
	/**
	 * Parses the specified JSON string obtained from the D&B REST service to a DetailedCompany object based on the specified basic Company object.
	 * 
	 * @param jsonResponseString	JSON string to parse (standardised by D&B)
	 * 
	 * @return						DetailedCompany object representing the company for the earlier performed look-up for which a detailed view was requested
	 */
	private DetailedCompany parseDetailedCompanyResult(String jsonResponseString) {		
		
		// Debug
//		System.out.println("Json Response String   <" + jsonResponseString + ">");
		
		// Get first JSON element with relevant information
		JsonObject jsonRoot    = new JsonParser().parse(jsonResponseString).getAsJsonObject();
		JsonObject companyRoot = jsonRoot.getAsJsonObject("OrderProductResponse").getAsJsonObject("OrderProductResponseDetail").
										  getAsJsonObject("Product").getAsJsonObject("Organization");
		
		// COMPANY
		// Summary
		JsonObject jsonSummary0    = companyRoot.getAsJsonObject("SubjectHeader");
		JsonPrimitive jsonSummary1 = (jsonSummary0 == null) ? null 	    : jsonSummary0.getAsJsonPrimitive("OrganizationSummaryText");
		String summary 			   = (jsonSummary1 == null) ? "unknown" : jsonSummary1.getAsString();
		JsonPrimitive jsonDUNS1	   = (jsonSummary0 == null) ? null		: jsonSummary0.getAsJsonPrimitive("DUNSNumber");
		String numberDUNS		   = (jsonDUNS1	   == null) ? "unknown" : jsonDUNS1.getAsString();
		
		// Sales revenue
		JsonObject jsonSales0    = companyRoot.getAsJsonObject("Financial");
		JsonArray jsonSales1     = (jsonSales0 == null) ? null : jsonSales0.getAsJsonArray("KeyFinancialFiguresOverview");
		JsonObject jsonSales2    = (jsonSales1 == null) ? null : jsonSales1.get(0).getAsJsonObject();
		JsonArray jsonSales3     = (jsonSales2 == null) ? null : jsonSales2.getAsJsonArray("SalesRevenueAmount");
		JsonObject financialRoot = (jsonSales3 == null) ? null : jsonSales3.get(0).getAsJsonObject();
		
		// OrganizationName
		JsonObject jsonOrgName0    = companyRoot.getAsJsonObject("OrganizationName");
		JsonArray jsonOrgName1     = (jsonOrgName0 == null) ? null : jsonOrgName0.getAsJsonArray("OrganizationPrimaryName");
		JsonObject jsonOrgName2    = (jsonOrgName1 == null) ? null : jsonOrgName1.get(0).getAsJsonObject();
		JsonObject jsonOrgNameX    = (jsonOrgName2 == null) ? null : jsonOrgName2.getAsJsonObject("OrganizationName");
		JsonPrimitive jsonOrgName3 = (jsonOrgNameX == null) ? null : jsonOrgNameX.getAsJsonPrimitive("$");
		String organizationName	   = (jsonOrgName3 == null) ? null : jsonOrgName3.getAsString();
				
		// Address
		JsonObject jsonRootAddress 		  = companyRoot.getAsJsonObject("Location");
		JsonArray jsonArrayPrimaryAddress = (jsonRootAddress == null) ? null : jsonRootAddress.getAsJsonArray("PrimaryAddress");
		
		String address = "";
		if (jsonArrayPrimaryAddress != null) {
			StringBuilder sbAddress 		= new StringBuilder();
			JsonObject jsonArrayPrimAddObj 	= (jsonArrayPrimaryAddress == null) ? null : jsonArrayPrimaryAddress.get(0).getAsJsonObject();
			JsonArray jsonArrayAddressParts = (jsonArrayPrimAddObj == null ) ? null : jsonArrayPrimAddObj.getAsJsonArray("StreetAddressLine");
			
			if (jsonArrayAddressParts == null)
				sbAddress.append("unknown");
			else {
				for (int j = 0; j < jsonArrayAddressParts.size(); j++) {
					JsonPrimitive jsonAddress = jsonArrayAddressParts.get(j).getAsJsonObject().getAsJsonPrimitive("LineText");
					sbAddress.append((jsonAddress == null) ? "unknown" : jsonAddress.getAsString());
				}
			}
			address = sbAddress.toString();
		}
		
		// Postal code
		JsonPrimitive jsonPostal = jsonRootAddress.getAsJsonPrimitive("PostalCode");
		String postalCode 	  	 = (jsonPostal == null) ? "uknown" : jsonPostal.getAsString();
		
		// City / town
		JsonPrimitive jsonPrimTown = jsonRootAddress.getAsJsonPrimitive("PrimaryTownName");
		String cityTown 		   = (jsonPrimTown == null) ? "uknown" : jsonPrimTown.getAsString();
		
		// Country ISO code
		JsonPrimitive jsonCountry = jsonRootAddress.getAsJsonPrimitive("CountryISOAlpha2Code");
		String countryISO 		  = (jsonCountry == null) ? "uknown" : jsonCountry.getAsString();
		
		
		JsonPrimitive jsonSalesRev = (financialRoot == null) ? null 	 : financialRoot.getAsJsonPrimitive("$");
		String salesRevenue 	   = (jsonSalesRev == null)  ? "unknown" : jsonSalesRev.getAsString();
		JsonPrimitive jsonCurrency = (financialRoot == null) ? null 	 : financialRoot.getAsJsonPrimitive("@CurrencyISOAlpha3Code");
		String currency			   = (jsonCurrency == null)  ? "" 		 : jsonCurrency.getAsString();

		// Organisation details
		// Start year
		JsonObject detailsRoot 	    = companyRoot.getAsJsonObject("OrganizationDetail");
		JsonPrimitive jsonStartYear = (detailsRoot == null)   ? null 	  : detailsRoot.getAsJsonPrimitive("OrganizationStartYear");
		String startYear	   		= (jsonStartYear == null) ? "unknown" : jsonStartYear.getAsString();
		
		// Standalone
		JsonPrimitive jsonStandalone = (detailsRoot == null) ? null : detailsRoot.getAsJsonPrimitive("StandaloneOrganizationIndicator");
		String isStandalone   		 = (jsonStandalone == null) ? "unknown" : jsonStandalone.getAsString();
		
		// Ownership type
		JsonObject jsonOwnership0    = (detailsRoot == null) ? null : detailsRoot.getAsJsonObject("ControlOwnershipTypeText");
		JsonPrimitive jsonOwnership1 = (jsonOwnership0 == null) ? null 		: jsonOwnership0.getAsJsonPrimitive("$");
		String ownerShip	   		 = (jsonOwnership1 == null) ? "unknown" : jsonOwnership1.getAsString();
		
		// Ownership since
		JsonObject jsonOwnerSince0    = (detailsRoot == null) ? null : detailsRoot.getAsJsonObject("ControlOwnershipDate");
		JsonPrimitive jsonOwnerSince1 = (jsonOwnerSince0 == null) ? null 		: jsonOwnerSince0.getAsJsonPrimitive("$");
		String ownerSince	   		  = (jsonOwnerSince1 == null) ? "unknown" : jsonOwnerSince1.getAsString();
		
		// Operating status
		JsonObject jsonOperating0 	 = (detailsRoot == null) ? null : detailsRoot.getAsJsonObject("OperatingStatusText");
		JsonPrimitive jsonOperating1 = (jsonOperating0 == null) ? null 		: jsonOperating0.getAsJsonPrimitive("$");
		String operatingStatus 		 = (jsonOperating1 == null) ? "unknown" : jsonOperating1.getAsString();
		
		// Legal form
		JsonObject jsonLegal0 	 = companyRoot.getAsJsonObject("RegisteredDetail");
		JsonObject jsonLegal1 	 = (jsonLegal0 == null) ? null 		: jsonLegal0.getAsJsonObject("LegalFormDetails");
		JsonObject jsonLegal2 	 = (jsonLegal1 == null) ? null 		: jsonLegal1.getAsJsonObject("LegalFormText");
		JsonPrimitive jsonLegal3 = (jsonLegal2 == null) ? null 		: jsonLegal2.getAsJsonPrimitive("$");
		String legalForm	     = (jsonLegal3 == null) ? "unknown" : jsonLegal3.getAsString();
		
		// Employee figures
		JsonObject jsonEmployee0    = companyRoot.getAsJsonObject("EmployeeFigures");
		JsonObject jsonEmployee1    = (jsonEmployee0 == null) ? null 		: jsonEmployee0.getAsJsonObject("IndividualEntityEmployeeDetails");
		JsonPrimitive jsonEmployee2 = (jsonEmployee1 == null) ? null 		: jsonEmployee1.getAsJsonPrimitive("TotalEmployeeQuantity");
		String amountOfEmployees    = (jsonEmployee2 == null) ? "unknown" : jsonEmployee2.getAsString();
		
		// Principal
		JsonObject jsonPrincipal0 	 = companyRoot.getAsJsonObject("PrincipalsAndManagement");
		JsonArray jsonPrincipal1  	 = (jsonPrincipal0 == null) ? null 		: jsonPrincipal0.getAsJsonArray("CurrentPrincipal");
		JsonObject jsonPrincipal2 	 = (jsonPrincipal1 == null) ? null 		: jsonPrincipal1.get(0).getAsJsonObject();
		JsonObject jsonPrincipal3 	 = (jsonPrincipal2 == null) ? null 		: jsonPrincipal2.getAsJsonObject("PrincipalName");
		JsonPrimitive jsonPrincipal4 = (jsonPrincipal3 == null) ? null 	    : jsonPrincipal3.getAsJsonPrimitive("FullName");
		String principal 			 = (jsonPrincipal4 == null) ? "unknown" : jsonPrincipal4.getAsString();

		// Global/Domestic ultimate organisations
		JsonObject linkageRoot 		   = companyRoot.getAsJsonObject("Linkage");
		JsonObject globalLinkageRoot   = (linkageRoot == null) ? null : linkageRoot.getAsJsonObject("GlobalUltimateOrganization");
		JsonObject domesticLinkageRoot = (linkageRoot == null) ? null : linkageRoot.getAsJsonObject("DomesticUltimateOrganization");
		
		// DUNS number
		JsonPrimitive jsonGlobal   = (globalLinkageRoot == null)   ? null 	   : globalLinkageRoot.getAsJsonPrimitive("DUNSNumber");
		String globalDUNS   	   = (jsonGlobal == null)   	   ? "unknown" : jsonGlobal.getAsString();
		JsonPrimitive jsonDomestic = (domesticLinkageRoot == null) ? null 	   : domesticLinkageRoot.getAsJsonPrimitive("DUNSNumber");
		String domesticDUNS 	   = (jsonDomestic == null) 	   ? null 	   : jsonDomestic.getAsString();
		
		// Company name
		JsonArray jsonCompGlobal0  		= (globalLinkageRoot == null)   ? null 	   	: globalLinkageRoot.getAsJsonArray("OrganizationPrimaryName");
		JsonObject jsonCompGlobal1 		= (jsonCompGlobal0 == null) 	? null 		: jsonCompGlobal0.get(0).getAsJsonObject();
		JsonObject jsonCompGlobal2 		= (jsonCompGlobal1 == null) 	? null 		: jsonCompGlobal1.getAsJsonObject("OrganizationName");
		JsonPrimitive jsonCompGlobal3   = (jsonCompGlobal2 == null) 	? null 		: jsonCompGlobal2.getAsJsonPrimitive("$");
		String globalName   		    = (jsonCompGlobal3 == null) 	? "unknown" : jsonCompGlobal3.getAsString();
		JsonArray jsonCompDomestic0     = (domesticLinkageRoot == null) ? null 	    : domesticLinkageRoot.getAsJsonArray("OrganizationPrimaryName");
		JsonObject jsonCompDomestic1    = (jsonCompDomestic0 == null) 	? null 		: jsonCompDomestic0.get(0).getAsJsonObject();
		JsonObject jsonCompDomestic2    = (jsonCompDomestic1 == null) 	? null 		: jsonCompDomestic1.getAsJsonObject("OrganizationName");
		JsonPrimitive jsonCompDomestic3 = (jsonCompDomestic2 == null) 	? null 		: jsonCompDomestic2.getAsJsonPrimitive("$");
		String domesticName 			= (jsonCompDomestic3 == null) 	? "unknown" : jsonCompDomestic3.getAsString();
		
		
		// Address
		JsonArray globalAddress0 	   = (globalLinkageRoot == null)   ? null 	   : globalLinkageRoot.getAsJsonArray("PrimaryAddress");
		JsonObject globalAddressRoot   = (globalAddress0 == null)      ? null 	   : globalAddress0.get(0).getAsJsonObject();
		JsonArray domesticAddress0 	   = (domesticLinkageRoot == null) ? null 	   : domesticLinkageRoot.getAsJsonArray("PrimaryAddress");
		JsonObject domesticAddressRoot = (domesticAddress0 == null)    ? null 	   : domesticAddress0.get(0).getAsJsonObject();
		JsonArray globalStreet0 	   = (globalAddressRoot == null)   ? null 	   : globalAddressRoot.getAsJsonArray("StreetAddressLine");
		JsonObject globalStreet1 	   = (globalStreet0 == null) 	   ? null 	   : globalStreet0.get(0).getAsJsonObject();
		JsonPrimitive globalStreet2    = (globalStreet1 == null) 	   ? null 	   : globalStreet1.getAsJsonPrimitive("LineText");
		String globalAddress   		   = (globalStreet2 == null) 	   ? "unknown" : globalStreet2.getAsString();
		JsonArray domesticStreet0 	   = (domesticAddressRoot == null) ? null 	   : domesticAddressRoot.getAsJsonArray("StreetAddressLine");
		JsonObject domesticStreet1 	   = (domesticStreet0 == null) 	   ? null 	   : domesticStreet0.get(0).getAsJsonObject();
		JsonPrimitive domesticStreet2  = (domesticStreet1 == null) 	   ? null 	   : domesticStreet1.getAsJsonPrimitive("LineText");
		String domesticAddress 		   = (domesticStreet2 == null) 	   ? "unknown" : domesticStreet2.getAsString();
		JsonPrimitive globalTown0 	   = (globalAddressRoot == null)   ? null 	   : globalAddressRoot.getAsJsonPrimitive("PrimaryTownName");
		String globalTown   		   = (globalTown0 == null) 		   ? "unknown" : globalTown0.getAsString();
		JsonPrimitive domesticTown0    = (domesticAddressRoot == null) ? null 	   : domesticAddressRoot.getAsJsonPrimitive("PrimaryTownName");
		String domesticTown 		   = (domesticTown0 == null) 	   ? "unknown" : domesticTown0.getAsString();
		JsonPrimitive globalCountry0   = (globalAddressRoot == null)   ? null 	   : globalAddressRoot.getAsJsonPrimitive("CountryISOAlpha2Code");
		String globalCountryCode   	   = (globalCountry0 == null) 	   ? "unknown" : globalCountry0.getAsString();
		JsonPrimitive domesticCountry0 = (domesticAddressRoot == null) ? null 	   : domesticAddressRoot.getAsJsonPrimitive("CountryISOAlpha2Code");
		String domesticCountryCode	   = (domesticCountry0 == null)    ? "unknown" : domesticCountry0.getAsString();
		JsonPrimitive globalPostal0    = (globalAddressRoot == null)   ? null 	   : globalAddressRoot.getAsJsonPrimitive("PostalCode");
		String globalPostalCode   	   = (globalPostal0 == null) 	   ? "unknown" : globalPostal0.getAsString();
		JsonPrimitive domesticPostal0  = (domesticAddressRoot == null) ? null 	   : domesticAddressRoot.getAsJsonPrimitive("PostalCode");
		String domesticPostalCode 	   = (domesticPostal0 == null) 	   ? "unknown" : domesticPostal0.getAsString();
		
		
		// Global ultimate company
		Company globalUltimate   = new Company(globalName,globalDUNS,globalAddress,globalPostalCode,globalTown,globalCountryCode);
		Company domesticUltimate = new Company(domesticName,domesticDUNS,domesticAddress,domesticPostalCode,domesticTown,domesticCountryCode);
		
		// Return new detailed company through its builder
		Company basicCompany = new Company(organizationName, numberDUNS, address, postalCode, cityTown, countryISO);
		return DetailedCompany.Builder.build(basicCompany).setSummary(summary).setSalesRevenue(salesRevenue).setCurrency(currency).setStartYear(startYear).
														   setStandaloneOrganization(isStandalone).setOwnership(ownerShip).setCurrentOwnerStartYear(ownerSince).
														   setOperatingStatus(operatingStatus).setLegalForm(legalForm).setAmountOfEmployees(amountOfEmployees).
														   setPrincipalFullName(principal).setGlobalUltimateOrganization(globalUltimate).setDomesticUltimateOrganization(domesticUltimate).get();
	}
	
	
	/**
	 * Parses the specified JSON string obtained from the D&B REST service to a list of BeneficialOwner objects
	 * 
	 * @param jsonResponseString	JSON string to parse (standardised by D&B)
	 * @return						List of BeneficialOwner objects representing the beneficial owners for the earlier looked-up company
	 */
	private List<BeneficialOwner> parseUBOResult(String jsonResponseString) {

		// Debug
//		System.out.println("Json Response String   <" + jsonResponseString + ">");
		
		List<BeneficialOwner> result = new ArrayList<>();
		
		// Get first JSON element with relevant information
		JsonObject jsonRoot = new JsonParser().parse(jsonResponseString).getAsJsonObject();
		JsonObject root = jsonRoot.getAsJsonObject("OrderProductResponse").getAsJsonObject("OrderProductResponseDetail").
								   getAsJsonObject("Product").getAsJsonObject("Organization").getAsJsonObject("Linkage").getAsJsonObject("BeneficialOwnership");
		
		
		// Arrays of beneficial owners and corresponding relationships to the company
		JsonArray beneficialOwners = root.getAsJsonArray("BeneficialOwners");
		JsonArray relationTypes    = root.getAsJsonArray("Relationships");
		
		// Beneficial owners
		for (int i = 0; i < beneficialOwners.size(); i++) {
			JsonObject beneficialOwner = beneficialOwners.get(i).getAsJsonObject();
			
			// Name
			JsonPrimitive jsonName = beneficialOwner.getAsJsonPrimitive("PrimaryName");
			String name 		   = (jsonName == null) ? "unknown" : jsonName.getAsString();
			
			// DUNS number
			JsonPrimitive jsonDUNS = beneficialOwner.getAsJsonPrimitive("DUNSNumber");
			String numberDUNS      = (jsonDUNS == null) ? "unknown" : jsonDUNS.getAsString();
			
			// Degree of separation
			JsonPrimitive jsonDegree = beneficialOwner.getAsJsonPrimitive("DegreeOfSeparation");
			int degreeOfSeparation   = (jsonDegree == null) ? -1 : Integer.parseInt(jsonDegree.getAsString());
			
			// Address
			JsonObject primAddressRoot = beneficialOwner.getAsJsonObject("PrimaryAddress");
			JsonArray addressRoot      = (primAddressRoot == null)  ? null 		: primAddressRoot.getAsJsonArray("StreetAddressLine");
			JsonPrimitive jsonAddress  = (addressRoot == null) 		? null 		: addressRoot.get(0).getAsJsonObject().getAsJsonPrimitive("LineText");
			String address     	       = (jsonAddress == null) 		? "unknown" : jsonAddress.getAsString();
			JsonPrimitive jsonTown     = (primAddressRoot == null)  ? null 		: primAddressRoot.getAsJsonPrimitive("PrimaryTownName");
			String town   	 	       = (jsonTown == null) 		? "unknown" : jsonTown.getAsString();
			JsonPrimitive jsonCountry  = (primAddressRoot == null)  ? null 		: primAddressRoot.getAsJsonPrimitive("CountryISOAlpha2Code");
			String countryCode 	       = (jsonCountry == null) 		? "unknown" : jsonCountry.getAsString();
			JsonPrimitive jsonPostal   = (primAddressRoot == null)  ? null 		: primAddressRoot.getAsJsonPrimitive("PostalCode");
			String postalCode  	       = (jsonPostal == null) 		? "unknown" : jsonPostal.getAsString();

			
			// Relationship
			JsonObject relationship = (relationTypes == null) ? null : relationTypes.get(i).getAsJsonObject();

			// Type
			JsonObject jsonTypeRoot = (relationship == null) ? null		 : relationship.getAsJsonObject("RelationshipTypeDescription");
			JsonPrimitive jsonType  = (jsonTypeRoot == null) ? null 	 : jsonTypeRoot.getAsJsonPrimitive("$");
			String relationshipType = (jsonType == null)     ? "unknown" : jsonType.getAsString();
			
			// Shareholding percentage
			JsonPrimitive jsonShareholding = (relationship == null)     ? null		: relationship.getAsJsonPrimitive("ShareHoldingPercentage");
			String shareHoldingPercentage  = (jsonShareholding == null) ? "unknown" : jsonShareholding.getAsString();
			
			// Add new BO to result builder
			result.add(new BeneficialOwner(name, numberDUNS, address, postalCode, town, countryCode, relationshipType, shareHoldingPercentage, degreeOfSeparation));
		}
		
		return result;
	}
}