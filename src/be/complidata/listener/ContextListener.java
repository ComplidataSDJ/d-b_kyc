package be.complidata.listener;

import java.io.IOException;
import java.util.Optional;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import be.complidata.communication.HttpsConnector;

@WebListener
public class ContextListener implements ServletContextListener {

   /**
     * @see ServletContextListener#contextInitialized(ServletContextEvent)
     */
    public void contextInitialized(ServletContextEvent contextEvent)  {
    	ServletContext context = contextEvent.getServletContext();
    	
        // Obtain and set auth token to context
    	Optional<String> token = null;
		try {
			token = new HttpsConnector().getAuthenticationToken();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if (!token.isPresent()) {
			// Set error message
			context.setAttribute("msgErrorAuthentication", "Failed to authenticate...");
			
			return;
		}
		
        context.setAttribute("token ",token );
       
    }

    
	/**
     * @see ServletContextListener#contextDestroyed(ServletContextEvent)
     */
    public void contextDestroyed(ServletContextEvent contextEvent)  {
    	System.out.println("Context destroyed!");
    }	
}