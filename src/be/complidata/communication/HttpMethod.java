package be.complidata.communication;

public enum HttpMethod {
	GET("GET"),
	POST("POST");
	
	// Immutable value
	private String value;
	
	
	// Private enum constructor
	private HttpMethod(String value) {
		this.value = value;
	}

	// Getter
	public String getValue() {
		return value;
	}
}