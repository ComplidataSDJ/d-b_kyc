package be.complidata.communication;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;

import javax.net.ssl.HttpsURLConnection;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class HttpsConnector {
	
	// Connection
	private final boolean CONN_USE_CACHE = false;
	private final boolean CONN_DO_INPUT  = true;
	
	// Header
	private final String HEADER_USER_AGENT 		   = "Mozilla/5.0";
	private final String HEADER_CONTENT_TYPE_JSON  = "application/json; charset=UTF-8";
	
	
	
	/**
	 * Method that can be used to authenticate with the D&B API (v2.0).
	 * If successful, returns the obtained authentication token; if unsuccessful, returns Optional.empty().
	 * 
	 * @throws IOException	Thrown when:
	 * 							- I/O exception occurred during writing the request
	 * 							- I/O exception occurred during reading the response
	 */
	public Optional<String> getAuthenticationToken() throws IOException {
		// Build URL
		URL url = new URL("https://direct.dnb.com/Authentication/V2.0/");
		
		// Build map of header parameters
		Map<String,String> paramsHeader = new HashMap<>();
		paramsHeader.put("x-dnb-user","P100000DFA8518AF3174653A16FFFC97");		// TODO when used in production, don't use hardcoded login credential
		paramsHeader.put("x-dnb-pwd","@S0mbeke13");
		
		// Try to connect to authentication service
		Optional<String> response = sendHttpsRequest(url, HttpMethod.POST, Optional.of(HEADER_USER_AGENT), Optional.of(HEADER_CONTENT_TYPE_JSON), paramsHeader, new HashMap<>());
		
		// Invalid -> return empty
		if (!response.isPresent())
			return Optional.empty();
		
		System.out.println("resp :  " + response.get());
		// Valid response at this point, create JSON object from response and extract authentication token
		JsonElement jsonResponse = new JsonParser().parse(response.get());
	    JsonObject jsonRoot = jsonResponse.getAsJsonObject();
	    String result = jsonRoot.getAsJsonObject("AuthenticationDetail").get("Token").getAsString();
	    
	    // Debug
	    //System.out.println("Result token =  " + result);
	    
	    return Optional.of(result);
	}
	
	
	/**
	 * Sends a new HTTP request to the specified URL and using one of the supported HTTP methods in HttpMethod.
	 * Furthermore, a map of header and body parameters (both of type String -> String) can be specified to add data to the request.
	 * In case of a GET-request, the body parameters will be added to the URL, in case of a POST request, they will be inside of the request body.
	 * 
	 * @param url					URL object representing the target to connect to
	 * @param method				HttpMethod enum object representing one of the supported HTTP methods
	 * @param optionalUserAgent		Optional String representing the user agent string to be set in the request header (uses default Mozilla 5.0 if none specified)
	 * @param optionalContentType	Optional String representing the standardised content-type string to be set in the request header (uses default JSON if none specified)
	 * @param paramsHeader			Map of String->String representing the header parameters to add to the request (key-value format)
	 * @param paramsBody			Map of String->String representing the body parameters to add to the request (key-value format)
	 * @return						Optional<String> object which is empty when failed to connect or when a non-200 HTTP response code was received,
	 * 								or filled with the actual response as a String object when the response code was equal to 200 (status=OK)
	 * @throws IOException			Thrown when:
	 * 									- Failed to connect to specified URL
	 * 									- Failed to set requested HTTP ConnectionMethod
	 * 									- I/O exception occurred during writing the request
	 * 									- I/O exception occurred during reading the response
	 */
	public Optional<String> sendHttpsRequest(URL url, HttpMethod method, Optional<String> optionalUserAgent, Optional<String> optionalContentType,
											 Map<String,String> paramsHeader, Map<String,String> paramsBody) throws IOException {
		
		// Set defaults where necessary
		String userAgent   = (!optionalUserAgent.isPresent())   ? HEADER_USER_AGENT : optionalUserAgent.get();
		String contentType = (!optionalContentType.isPresent()) ? HEADER_CONTENT_TYPE_JSON : optionalContentType.get();
		

		// Handle request based on the requested HTTP method
		HttpsURLConnection connection;
		switch(method) {
			case GET :
				connection = createHttpsGetRequest(url, userAgent, contentType, paramsHeader, paramsBody);
				break;
			case POST :
				connection = createHttpsPostRequest(url, userAgent, contentType, paramsHeader, paramsBody);
				break;
			default:
				return Optional.empty();
		}

		// Execute request and return response
		return this.handleResponse(connection);
	}
	
	
	
	
	// GET (private)
	private HttpsURLConnection createHttpsGetRequest(URL url, String userAgent, String contentType, Map<String,String> paramsHeader, Map<String,String> paramsBody) throws IOException {
		// Append body parameters to URL (cf. GET approach)
		String bodyParams = buildQueryString(paramsBody);
		url = (bodyParams.length() > 0) ? new URL(url.toString() + "?" + bodyParams) : url;
		
		// Create connection object
		HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
		
		// Connection properties
		connection.setUseCaches(CONN_USE_CACHE);
		connection.setDoInput(CONN_DO_INPUT);
		connection.setDoOutput(false);
		
		// Request header
		connection.setRequestMethod("GET");
		connection.setRequestProperty("User-Agent",userAgent);
		connection.setRequestProperty("Content-Type",contentType);
		
		// Header parameters
		for (Entry<String, String> entry : paramsHeader.entrySet())
			connection.setRequestProperty(entry.getKey(), entry.getValue());

		// Return connection (GET request)
		return connection;
	}

	
	// POST (private)
	private HttpsURLConnection createHttpsPostRequest(URL url, String userAgent, String contentType, Map<String,String> paramsHeader, Map<String,String> paramsBody) throws IOException {
		// Create connection object
		HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
		
		// Connection properties
		connection.setUseCaches(CONN_USE_CACHE);
		connection.setDoInput(CONN_DO_INPUT);
		connection.setDoOutput(true);
		
		// Request header
		connection.setRequestMethod("POST");
		connection.setRequestProperty("User-Agent",userAgent);
		connection.setRequestProperty("Content-Type",contentType);
		
		// Header parameters
		for (Entry<String, String> entry : paramsHeader.entrySet())
			connection.setRequestProperty(entry.getKey(), entry.getValue());
		
		// Body parameters (cf. POST method)
		String bodyParams = buildQueryString(paramsBody);
		OutputStream os = connection.getOutputStream();
		BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
		writer.write(bodyParams);
		writer.flush();
		writer.close();

		// Return connection (POST request)
		return connection;
	}
	
	
	/**
	 * Private method which builds an encoded query string (supported for all HTTP request methods) from the specified map which can be sent in a new request.
	 * Parameters should be specified as key-value pairs in the map of String->String objects.
	 * 
	 * @param params							Map of String->String objects representing the key-value parameter pairs
	 * @return									Query string holding the requested parameters and which can be used to send in a request
	 * @throws UnsupportedEncodingException		Thrown when failed to encode either the key or value of a parameter pair
	 */
	private String buildQueryString(Map<String,String> params) throws UnsupportedEncodingException {
	    StringBuilder result = new StringBuilder();
	    boolean first = true;

	    // For every parameter in the map
	    for (Entry<String, String> entry : params.entrySet()) {
	    	
	    	// Check if adding first parameter (if not, concatenate with ampersand)
	        if (first)
	            first = false;
	        else
	            result.append("&");

	        // Build "key=value" format with UTF-8 encoded strings
	        result.append(URLEncoder.encode(entry.getKey(), "UTF-8").replace("+", "%20")).
	        	   append("=").
	        	   append(URLEncoder.encode(entry.getValue(), "UTF-8").replace("+", "%20"));
	    }

	    return result.toString();
	}

	
	
	private Optional<String> handleResponse(HttpsURLConnection connection) throws IOException {
		// Response reader (buffered) based on response code (i.e. success / failure)
		int responseCode = connection.getResponseCode();
		BufferedReader in = (200 <= responseCode && responseCode <= 299) ? new BufferedReader(new InputStreamReader(connection.getInputStream())) :
																		   new BufferedReader(new InputStreamReader(connection.getErrorStream()));
		StringBuilder responseBuilder = new StringBuilder();
		String line;
		while ((line = in.readLine()) != null) {
			System.out.println(line);
			responseBuilder.append(line);
		}
		in.close();
		
		// Invalid -> return empty
		if (responseCode != 200)
			return Optional.empty();
		
		// Valid at this point -> return response
		return Optional.of(responseBuilder.toString());
	}
}