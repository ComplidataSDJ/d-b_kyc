package be.complidata.io;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;

public class WriterExcel {
	
	public void export(List<List<String>> documents, File outFile) {
        // Create Excel workbook
        HSSFWorkbook workbook = new HSSFWorkbook();
        HSSFSheet sheet       = workbook.createSheet("Output data");

        // Write data to Excel workbook
        int rowCounter = 0;
        for (List<String> rowData : documents){
            Row row = sheet.createRow(rowCounter++);
            int colCounter = 0;

            for (String field : rowData)
                row.createCell(colCounter++).setCellValue(field);
        }

        // Write Excel workbook to specified output file
        try {
            FileOutputStream outputStream = new FileOutputStream(outFile);
            workbook.write(outputStream);
            workbook.close();
        } catch (FileNotFoundException e) {
        	System.out.println("Error:  file not found while trying to export data as Excel file:" + e.getMessage());
        } catch (IOException e) {
        	System.out.println("Error:  file exists, but IOException occurred while trying to export data as Excel file:" + e.getMessage());
        }
	}
}
