package be.complidata.models;

public class BeneficialOwner extends Company {
	private String relationType;
	private String shareHoldingPercentage;
	private int degreeOfSeparation;
	
	
	public BeneficialOwner(String name, String numberDUNS, String address, String postalCode, String cityTown, String countryISO,
						   String relationType, String shareHoldingPercentage, int degreeOfSeparation) {
		super(name, numberDUNS, address, postalCode, cityTown, countryISO);
		
		this.relationType = relationType;
		this.shareHoldingPercentage = shareHoldingPercentage;
		this.degreeOfSeparation = degreeOfSeparation;
	}
	
	public String getRelationType() {
		return relationType;
	}
	
	public String getShareHoldingPercentage() {
		return shareHoldingPercentage;
	}
	
	public int getDegreeOfSeparation() {
		return degreeOfSeparation;
	}
}
