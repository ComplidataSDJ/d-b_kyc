package be.complidata.models;

/**
 * Builder pattern for constructing detailed companies with the required base parameters for instantiating a Company
 * and building methods for all other fields.
 */
public class DetailedCompany extends Company {

	private String summary;
	private String salesRevenue;
	private String currency;
	private String standaloneOrganization;
	private String operatingStatus;
	private String startYear;
	private String ownership;
	private String currentOwnerStartYear;
	private String legalForm;
	private String amountOfEmployees;
	private String principalFullName;
	private Company globalUltimateOrganization;
	private Company domesticUltimateOrganization;

	
	DetailedCompany(String name, String numberDUNS, String address, String postalCode, String cityTown, String countryISO) {
		super(name, numberDUNS, address, postalCode, cityTown, countryISO);
	}
	
	public static class Builder {
		private DetailedCompany companyToBuild;
		
		private Builder(final DetailedCompany company) {
			companyToBuild = company;
        }

		public static Builder build(Company basicCompany) {
			return new Builder(new DetailedCompany(basicCompany.getName(), basicCompany.getNumberDUNS(), basicCompany.getAddress(),
												   basicCompany.getPostalCode(), basicCompany.getCityTown(), basicCompany.getCountryISO()));
        }
		
		public Builder setSummary(String summary) {
			this.companyToBuild.summary = summary;
			return this;
		}

		public Builder setSalesRevenue(String salesRevenue) {
			this.companyToBuild.salesRevenue = salesRevenue;
			return this;
		}

		public Builder setCurrency(String currency) {
			this.companyToBuild.currency = currency;
			return this;
		}

		public Builder setStandaloneOrganization(String isStandaloneOrganization) {
			this.companyToBuild.standaloneOrganization = isStandaloneOrganization;
			return this;
		}

		public Builder setOperatingStatus(String operatingStatus) {
			this.companyToBuild.operatingStatus = operatingStatus;
			return this;
		}

		public Builder setStartYear(String startYear) {
			this.companyToBuild.startYear = startYear;
			return this;
		}

		public Builder setOwnership(String ownerShip) {
			this.companyToBuild.ownership = ownerShip;
			return this;
		}

		public Builder setCurrentOwnerStartYear(String currentOwnerStartYear) {
			this.companyToBuild.currentOwnerStartYear = currentOwnerStartYear;
			return this;
		}

		public Builder setLegalForm(String legalForm) {
			this.companyToBuild.legalForm = legalForm;
			return this;
		}

		public Builder setAmountOfEmployees(String amountOfEmployees) {
			this.companyToBuild.amountOfEmployees = amountOfEmployees;
			return this;
		}

		public Builder setPrincipalFullName(String principalFullName) {
			this.companyToBuild.principalFullName = principalFullName;
			return this;
		}

		public Builder setGlobalUltimateOrganization(Company globalUltimateOrganization) {
			this.companyToBuild.globalUltimateOrganization = globalUltimateOrganization;
			return this;
		}

		public Builder setDomesticUltimateOrganization(Company domesticUltimateOrganization) {
			this.companyToBuild.domesticUltimateOrganization = domesticUltimateOrganization;
			return this;
		}
		
        public DetailedCompany get() {
            return companyToBuild;
        }
	}

	public String getSummary() {
		return summary;
	}

	public String getSalesRevenue() {
		return salesRevenue;
	}

	public String getCurrency() {
		return currency;
	}

	public String getStandaloneOrganization() {
		return standaloneOrganization;
	}

	public String getOperatingStatus() {
		return operatingStatus;
	}

	public String getStartYear() {
		return startYear;
	}

	public String getOwnership() {
		return ownership;
	}

	public String getCurrentOwnerStartYear() {
		return currentOwnerStartYear;
	}

	public String getLegalForm() {
		return legalForm;
	}

	public String getAmountOfEmployees() {
		return amountOfEmployees;
	}

	public String getPrincipalFullName() {
		return principalFullName;
	}

	public Company getGlobalUltimateOrganization() {
		return globalUltimateOrganization;
	}

	public Company getDomesticUltimateOrganization() {
		return domesticUltimateOrganization;
	}
}
