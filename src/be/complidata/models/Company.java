package be.complidata.models;

public class Company {

	private String name;
	private String numberDUNS;
	private String address;
	private String postalCode;
	private String cityTown;
	private String countryISO;
	
	
	public Company(String name, String numberDUNS, String address, String postalCode, String cityTown, String countryISO) {
		this.name 		 = name;
		this.numberDUNS  = numberDUNS;
		this.address 	 = address;
		this.postalCode  = postalCode;
		this.cityTown	 = cityTown;
		this.countryISO  = countryISO;
	}

	public String getName() {
		return name;
	}

	public String getNumberDUNS() {
		return numberDUNS;
	}
	
	public String getAddress() {
		return address;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public String getCityTown() {
		return cityTown;
	}

	public String getCountryISO() {
		return countryISO;
	}
	
	@Override
	public String toString() {
		return new StringBuilder("\nCompany Details:").
						  append("\n================").
				   		  append("\n\tDUNS number :  " + getNumberDUNS()).
				   		  append("\n\tName        :  " + getName()).
				   		  append("\n\tAddress     :  " + getAddress()).
				   		  append("\n\tPostal code :  " + getPostalCode()).
				   		  append("\n\tCity / town :  " + getCityTown()).
				   		  append("\n\tCountry ISO :  " + getCountryISO()).toString();
	}
}
