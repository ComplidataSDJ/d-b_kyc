package be.complidata.servlets.pages;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Root servlet that simply loads homepage while encapsulating JSP
 */
@WebServlet("/root")
public class RootServlet extends HttpServlet {
	private static final long serialVersionUID = 1753167770861669399L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Forward to view
		getServletContext().getRequestDispatcher("/WEB-INF/index.jsp").forward(request, response);
	}
}
