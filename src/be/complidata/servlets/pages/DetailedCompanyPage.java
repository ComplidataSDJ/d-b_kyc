package be.complidata.servlets.pages;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Company details servlet that simply loads view while encapsulating JSP
 */
@WebServlet("/company_details")
public class DetailedCompanyPage extends HttpServlet {
	private static final long serialVersionUID = -2293270990004008795L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Forward to view
		getServletContext().getRequestDispatcher("/WEB-INF/company_details.jsp").forward(request, response);
	}
}
