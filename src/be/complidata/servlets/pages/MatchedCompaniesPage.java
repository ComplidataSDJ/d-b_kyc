package be.complidata.servlets.pages;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Root servlet that simply loads view while encapsulating JSP
 */
@WebServlet("/matched_companies")
public class MatchedCompaniesPage extends HttpServlet {
	private static final long serialVersionUID = -2293270990004008795L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Forward to view
		getServletContext().getRequestDispatcher("/WEB-INF/matched_companies.jsp").forward(request, response);
	}
}
