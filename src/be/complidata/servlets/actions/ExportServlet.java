package be.complidata.servlets.actions;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.FileUtils;

import be.complidata.io.WriterExcel;
import be.complidata.models.BeneficialOwner;
import be.complidata.utilities.Constants;
import be.complidata.utilities.Functions;


@WebServlet("/export")
public class ExportServlet extends HttpServlet {
	private static final long serialVersionUID = -1348373561331170287L;


	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		
		// Obtain data to export and convert properties to String objects
		@SuppressWarnings("unchecked") List<BeneficialOwner> exportData = (List<BeneficialOwner>) session.getAttribute("beneficialOwners");
		List<List<String>> outputData = Functions.prepareForExport(exportData);
		for(List<String> listString : outputData) {
			System.out.println(listString);
		}
		
		// Write to Excel file (.xls)
		File fileOut = new File(Constants.PATH_EXPORT);
		new WriterExcel().export(outputData, fileOut);
		
		// Response configuration
        response.setHeader("Content-Disposition","filename=\"" + fileOut.getName() + "\"");
        response.setContentType("application/octet-stream");
        
        // Write excel file to output stream
        FileUtils.copyFile(fileOut,response.getOutputStream());
		return;
	}
}
