package be.complidata.servlets.actions;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;

import be.complidata.communication.HttpMethod;
import be.complidata.communication.HttpsConnector;
import be.complidata.models.BeneficialOwner;
import be.complidata.models.DetailedCompany;
import be.complidata.utilities.Constants;


@WebServlet("/beneficial_owners_lookup")
public class BeneficialOwnersLookupServlet extends HttpServlet {
	private static final long serialVersionUID = -2293270990004008795L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		HttpSession session = request.getSession();
		
		// Retrieve Company object which the client selected
		DetailedCompany company = (DetailedCompany) session.getAttribute("detailedCompany");
		
		// Invalid -> return
		if (company == null)
			return;
		
		// Obtain DUNS number
		String numberDUNS = company.getNumberDUNS();
		
		// Header parameters
		Map<String,String> paramsHeader = new HashMap<>();
		paramsHeader.put("Authorization", (String)session.getAttribute("authToken"));
		
		// Body parameters
		Map<String,String> paramsBody = new HashMap<>();
		paramsBody.put("OwnershipType", "BENF_OWRP");
		paramsBody.put("OwnershipPercentage", "0");

		// Retrieve detailed company information
		HttpsConnector connector = new HttpsConnector();
		URL url = new URL("https://direct.dnb.com/V6.0/organizations/" + numberDUNS +"/products/CMP_BOL");
		Optional<String> responseString = connector.sendHttpsRequest(url, HttpMethod.GET, Optional.empty(), Optional.empty(), paramsHeader, paramsBody);
		
		// Parse response and handle result
		// Failed
		if (!responseString.isPresent()) {
			// Set error message and forward to view (client refresh will re-send lookup request)
			request.setAttribute("msgErrorBeneficialOwners", "Failed to retrieve beneficial owners...");
			getServletContext().getRequestDispatcher("/company_details").forward(request, response);
			return;
		}

		
		// Valid at this point -> set result as session attribute and redirect to GET (PRG-pattern)
		List<BeneficialOwner> beneficialOwners = parseResult(responseString.get());
		session.setAttribute("beneficialOwners", beneficialOwners);
		response.sendRedirect("/export");		/* TODO this just calls excel download, not sure what should happen in front-end */
	}

	
	
	/**
	 * Parses the specified JSON string obtained from the D&B REST service to a list of BeneficialOwner objects
	 * 
	 * @param jsonResponseString	JSON string to parse (standardised by D&B)
	 * @return						List of BeneficialOwner objects representing the beneficial owners for the earlier looked-up company
	 */
	private List<BeneficialOwner> parseResult(String jsonResponseString) {
		List<BeneficialOwner> result = new ArrayList<>();
		
		// Get first JSON element with relevant information
		JsonObject jsonRoot = new JsonParser().parse(jsonResponseString).getAsJsonObject();
		JsonObject root = jsonRoot.getAsJsonObject("OrderProductResponse").getAsJsonObject("OrderProductResponseDetail").
								   getAsJsonObject("Product").getAsJsonObject("Organization").getAsJsonObject("Linkage").getAsJsonObject("BeneficialOwnership");
		
		
		// Arrays of beneficial owners and corresponding relationships to the company
		JsonArray beneficialOwners = root.getAsJsonArray("BeneficialOwners");
		JsonArray relationTypes    = root.getAsJsonArray("Relationships");
		
		// Beneficial owners
		for (int i = 0; i < beneficialOwners.size(); i++) {
			JsonObject beneficialOwner = beneficialOwners.get(i).getAsJsonObject();
			
			// Name
			JsonPrimitive jsonName = beneficialOwner.getAsJsonPrimitive("PrimaryName");
			String name 		   = (jsonName == null) ? "unknown" : jsonName.getAsString();
			
			// DUNS number
			JsonPrimitive jsonDUNS = beneficialOwner.getAsJsonPrimitive("DUNSNumber");
			String numberDUNS      = (jsonDUNS == null) ? "unknown" : jsonDUNS.getAsString();
			
			// Degree of separation
			JsonPrimitive jsonDegree = beneficialOwner.getAsJsonPrimitive("DegreeOfSeparation");
			int degreeOfSeparation   = (jsonDegree == null) ? -1 : Integer.parseInt(jsonDegree.getAsString());
			
			// Address
			JsonObject primAddressRoot = beneficialOwner.getAsJsonObject("PrimaryAddress");
			JsonArray addressRoot      = (primAddressRoot == null)  ? null 		: primAddressRoot.getAsJsonArray("StreetAddressLine");
			JsonPrimitive jsonAddress  = (addressRoot == null) 		? null 		: addressRoot.get(0).getAsJsonObject().getAsJsonPrimitive("LineText");
			String address     	       = (jsonAddress == null) 		? "unknown" : jsonAddress.getAsString();
			JsonPrimitive jsonTown     = (primAddressRoot == null)  ? null 		: primAddressRoot.getAsJsonPrimitive("PrimaryTownName");
			String town   	 	       = (jsonTown == null) 		? "unknown" : jsonTown.getAsString();
			JsonPrimitive jsonCountry  = (primAddressRoot == null)  ? null 		: primAddressRoot.getAsJsonPrimitive("CountryISOAlpha2Code");
			String countryCode 	       = (jsonCountry == null) 		? "unknown" : jsonCountry.getAsString();
			JsonPrimitive jsonPostal   = (primAddressRoot == null)  ? null 		: primAddressRoot.getAsJsonPrimitive("PostalCode");
			String postalCode  	       = (jsonPostal == null) 		? "unknown" : jsonPostal.getAsString();

			
			// Relationship
			JsonObject relationship = (relationTypes == null) ? null : relationTypes.get(i).getAsJsonObject();

			// Type
			JsonObject jsonTypeRoot = (relationship == null) ? null		 : relationship.getAsJsonObject("RelationshipTypeDescription");
			JsonPrimitive jsonType  = (jsonTypeRoot == null) ? null 	 : jsonTypeRoot.getAsJsonPrimitive("$");
			String relationshipType = (jsonType == null)     ? "unknown" : jsonType.getAsString();
			
			// Shareholding percentage
			JsonPrimitive jsonShareholding = (relationship == null)     ? null		: relationship.getAsJsonPrimitive("ShareHoldingPercentage");
			String shareHoldingPercentage  = (jsonShareholding == null) ? "unknown" : jsonShareholding.getAsString();
			
			// Add new BO to result builder
			result.add(new BeneficialOwner(name, numberDUNS, address, postalCode, town, countryCode, relationshipType, shareHoldingPercentage, degreeOfSeparation));
		}
		
		return result;
	}
}
