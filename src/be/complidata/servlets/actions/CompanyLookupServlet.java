package be.complidata.servlets.actions;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;

import be.complidata.communication.HttpMethod;
import be.complidata.communication.HttpsConnector;
import be.complidata.models.Company;

/**
 * Servlet implementation class MatthiasServlet
 */
@WebServlet("/company_lookup")
public class CompanyLookupServlet extends HttpServlet {
	private static final long serialVersionUID = -2293270990004008795L;
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Forward to view
		getServletContext().getRequestDispatcher("/WEB-INF/company_lookup.jsp").forward(request, response);
	}
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		HttpSession session = request.getSession();
		
		// Obtain request parameters
		String companyNameReq = request.getParameter("txtCompanyName");
		String countryCodeReq = request.getParameter("txtCountryISO");
		
		// Invalid -> return
		if (companyNameReq == null || countryCodeReq == null || countryCodeReq.length() != 2) {
			request.setAttribute("msgErrorCompanyLookup", "Failed to look up company, make sure to specify both company name and country code. Furthermore, country codes should be 2 characters long as specified by the ISO standard.");
			request.getServletContext().getRequestDispatcher("/WEB-INF/company_lookup.jsp").forward(request, response);
			return;
		}
		
		// Header parameters
		Map<String,String> paramsHeader = new HashMap<>();
		paramsHeader.put("Authorization", (String)session.getAttribute("authToken"));
		
		// Body parameters
		Map<String,String> paramsBody = new HashMap<>();
		paramsBody.put("SubjectName", companyNameReq);
		paramsBody.put("CountryISOAlpha2Code", countryCodeReq);
		paramsBody.put("match", "true");
//		paramsBody.put("MatchTypeText", "Simple");

		// Retrieve simple company information
		HttpsConnector connector = new HttpsConnector();
		URL url = new URL("https://direct.dnb.com/V6.0/organizations");
		Optional<String> responseString = connector.sendHttpsRequest(url, HttpMethod.GET, Optional.empty(), Optional.empty(), paramsHeader, paramsBody);
		
		// Parse response and handle result
		// Failed
		if (!responseString.isPresent()) {
			// Set error message and forward to view (client refresh will re-send lookup request)
			request.setAttribute("msgErrorCompanyLookup", "Failed to look up company...");
			request.getServletContext().getRequestDispatcher("/WEB-INF/company_lookup.jsp").forward(request, response);
			return;
		}
		
		System.out.println(responseString);
		
		// Valid at this point -> set result as session attribute and redirect (PRG-pattern)
		List<Company> matchedCompanies = parseMatchedCompanyResult(responseString.get());
		session.setAttribute("matchedCompanies", matchedCompanies);
		response.sendRedirect("/matched_companies");
	}

	
	
	/**
	 * Parses the specified JSON string obtained from the D&B REST service to MatchedCompany objects.
	 * 
	 * @param jsonResponseString	JSON string to parse (standardised by D&B)
	 * @return						List of MatchedCompany objects representing the possible matched companies for the earlier performed look-up
	 */
	private List<Company> parseMatchedCompanyResult(String jsonResponseString) {
		List<Company> result = new ArrayList<>();
		
		// For all matched candidate companies in the specified JSON response string
		JsonObject jsonRoot = new JsonParser().parse(jsonResponseString).getAsJsonObject();
		JsonArray matchCandidates = jsonRoot.getAsJsonObject("MatchResponse").getAsJsonObject("MatchResponseDetail").getAsJsonArray("MatchCandidate");
		for (int i = 0; i < matchCandidates.size(); i++) {
			
			// JSON root of current matched candidate
			JsonObject currentCandidate = matchCandidates.get(i).getAsJsonObject();

			// DUNS number
			JsonPrimitive jsonDUNS = currentCandidate.getAsJsonPrimitive("DUNSNumber");
			String numberDUNS 	   = (jsonDUNS == null) ? "unknown" : jsonDUNS.getAsString();
			
			
			// Company name
			JsonObject jsonOrgPrimName = currentCandidate.getAsJsonObject("OrganizationPrimaryName");
			JsonObject jsonOrgName 	   = (jsonOrgPrimName == null) ? null      : jsonOrgPrimName.getAsJsonObject("OrganizationName");
			String companyName 		   = (jsonOrgName == null) 	   ? "unknown" : jsonOrgName.getAsJsonPrimitive("$").getAsString();

			// Phone number
//			JsonObject jsonRootPhone = currentCandidate.getAsJsonObject("TelephoneNumber");
//			String phoneNumber = jsonRootPhone.getAsJsonPrimitive("TelecommunicationNumber").getAsString();
			
			
			// Address
			JsonObject jsonRootAddress = currentCandidate.getAsJsonObject("PrimaryAddress");
			if (jsonRootAddress == null)
				continue;
			
			StringBuilder sbAddress = new StringBuilder();
			JsonArray jsonArrayAddressParts = jsonRootAddress.getAsJsonArray("StreetAddressLine");
			
			if (jsonArrayAddressParts == null)
				sbAddress.append("unknown");
			else {
				for (int j = 0; j < jsonArrayAddressParts.size(); j++) {
					JsonPrimitive jsonAddress = jsonArrayAddressParts.get(j).getAsJsonObject().getAsJsonPrimitive("LineText");
					sbAddress.append((jsonAddress == null) ? "unknown" : jsonAddress.getAsString());
				}
			}
			String address = sbAddress.toString();
			
			// Postal code
			JsonPrimitive jsonPostal = jsonRootAddress.getAsJsonPrimitive("PostalCode");
			String postalCode 	  	 = (jsonPostal == null) ? "uknown" : jsonPostal.getAsString();
			
			// City / town
			JsonPrimitive jsonPrimTown = jsonRootAddress.getAsJsonPrimitive("PrimaryTownName");
			String cityTown 		   = (jsonPrimTown == null) ? "uknown" : jsonPrimTown.getAsString();
			
			// Country ISO code
			JsonPrimitive jsonCountry = jsonRootAddress.getAsJsonPrimitive("CountryISOAlpha2Code");
			String countryISO 		  = (jsonCountry == null) ? "uknown" : jsonCountry.getAsString();
			
			
			// Add candidate company to result builder
			result.add(new Company(companyName, numberDUNS, address, postalCode, cityTown, countryISO));
		}
		
		return result;
	}
}
