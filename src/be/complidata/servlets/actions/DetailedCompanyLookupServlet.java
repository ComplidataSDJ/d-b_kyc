    package be.complidata.servlets.actions;

import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;

import be.complidata.communication.HttpMethod;
import be.complidata.communication.HttpsConnector;
import be.complidata.models.Company;
import be.complidata.models.DetailedCompany;


@WebServlet("/company_details_lookup")
public class DetailedCompanyLookupServlet extends HttpServlet {
	private static final long serialVersionUID = -2293270990004008795L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		HttpSession session = request.getSession();
		
		// Obtain request parameter
		String numberDUNS = request.getParameter("numberDUNS");
		
		// Retrieve Company object which the client selected
		@SuppressWarnings("unchecked") List<Company> matchedCompanies = (List<Company>) session.getAttribute("matchedCompanies");
		Company requestedCompany = null;
		for (Company company : matchedCompanies) {
			if (!company.getNumberDUNS().equals(numberDUNS))
				continue;
			
			// Match found -> set as found company and break loop
			requestedCompany = company;
			break;
		}
		
		// Invalid -> return
		if (requestedCompany == null)
			return;
		
		// Header parameters
		Map<String,String> paramsHeader = new HashMap<>();
		paramsHeader.put("Authorization", (String)session.getAttribute("authToken"));
		
		// Body parameters
		Map<String,String> paramsBody = new HashMap<>();
		paramsBody.put("DUNSNumber", numberDUNS);
		
		// Retrieve detailed company information
		HttpsConnector connector = new HttpsConnector();
		URL url = new URL("https://direct.dnb.com/V7.0/organizations/" + numberDUNS +"/products/DCP_PREM");
		Optional<String> responseString = connector.sendHttpsRequest(url, HttpMethod.GET, Optional.empty(), Optional.empty(), paramsHeader, paramsBody);
		
		// Parse response and handle result
		// Failed
		if (!responseString.isPresent()) {
			// Set error message and forward to view (client refresh will re-send lookup request)
			request.setAttribute("msgErrorCompanyDetails", "Failed to retrieve company details...");
			getServletContext().getRequestDispatcher("/matched_companies").forward(request, response);
			return;
		}
		
		

		
		// Valid at this point -> set result as session attribute and redirect to GET (PRG-pattern)
		DetailedCompany detailedCompany = parseResult(requestedCompany, responseString.get());
		session.setAttribute("detailedCompany", detailedCompany);
		response.sendRedirect("/company_details");
	}

	
	
	/**
	 * Parses the specified JSON string obtained from the D&B REST service to a DetailedCompany object based on the specified basic Company object.
	 * 
	 * @param jsonResponseString	JSON string to parse (standardised by D&B)
	 * @return						DetailedCompany objects representing the company for the earlier performed look-up for which a detailed view was requested
	 */
	private DetailedCompany parseResult(Company basicCompany, String jsonResponseString) {		
		// Get first JSON element with relevant information
		JsonObject jsonRoot    = new JsonParser().parse(jsonResponseString).getAsJsonObject();
		JsonObject companyRoot = jsonRoot.getAsJsonObject("OrderProductResponse").getAsJsonObject("OrderProductResponseDetail").
										  getAsJsonObject("Product").getAsJsonObject("Organization");
		
		// COMPANY
		// Summary
		JsonObject jsonSummary0    = companyRoot.getAsJsonObject("SubjectHeader");
		JsonPrimitive jsonSummary1 = (jsonSummary0 == null) ? null 	    : jsonSummary0.getAsJsonPrimitive("OrganizationSummaryText");
		String summary 			   = (jsonSummary1 == null) ? "unknown" : jsonSummary1.getAsString();
		
		// Sales revenue
		JsonObject jsonSales0    = companyRoot.getAsJsonObject("Financial");
		JsonArray jsonSales1     = (jsonSales0 == null) ? null : jsonSales0.getAsJsonArray("KeyFinancialFiguresOverview");
		JsonObject jsonSales2    = (jsonSales1 == null) ? null : jsonSales1.get(0).getAsJsonObject();
		JsonArray jsonSales3     = (jsonSales2 == null) ? null : jsonSales2.getAsJsonArray("SalesRevenueAmount");
		JsonObject financialRoot = (jsonSales3 == null) ? null : jsonSales3.get(0).getAsJsonObject();
		
		JsonPrimitive jsonSalesRev = (financialRoot == null) ? null 	 : financialRoot.getAsJsonPrimitive("$");
		String salesRevenue 	   = (jsonSalesRev == null)  ? "unknown" : jsonSalesRev.getAsString();
		JsonPrimitive jsonCurrency = (financialRoot == null) ? null 	 : financialRoot.getAsJsonPrimitive("@CurrencyISOAlpha3Code");
		String currency			   = (jsonCurrency == null)  ? "" 		 : jsonCurrency.getAsString();

		// Organisation details
		// Start year
		JsonObject detailsRoot 	    = companyRoot.getAsJsonObject("OrganizationDetail");
		JsonPrimitive jsonStartYear = (detailsRoot == null)   ? null 	  : detailsRoot.getAsJsonPrimitive("OrganizationStartYear");
		String startYear	   		= (jsonStartYear == null) ? "unknown" : jsonStartYear.getAsString();
		
		// Standalone
		JsonPrimitive jsonStandalone = (detailsRoot == null) ? null : detailsRoot.getAsJsonPrimitive("StandaloneOrganizationIndicator");
		String isStandalone   		 = (jsonStandalone == null) ? "unknown" : jsonStandalone.getAsString();
		
		// Ownership type
		JsonObject jsonOwnership0    = (detailsRoot == null) ? null : detailsRoot.getAsJsonObject("ControlOwnershipTypeText");
		JsonPrimitive jsonOwnership1 = (jsonOwnership0 == null) ? null 		: jsonOwnership0.getAsJsonPrimitive("$");
		String ownerShip	   		 = (jsonOwnership1 == null) ? "unknown" : jsonOwnership1.getAsString();
		
		// Ownership since
		JsonObject jsonOwnerSince0    = (detailsRoot == null) ? null : detailsRoot.getAsJsonObject("ControlOwnershipDate");
		JsonPrimitive jsonOwnerSince1 = (jsonOwnerSince0 == null) ? null 		: jsonOwnerSince0.getAsJsonPrimitive("$");
		String ownerSince	   		  = (jsonOwnerSince1 == null) ? "unknown" : jsonOwnerSince1.getAsString();
		
		// Operating status
		JsonObject jsonOperating0 	 = (detailsRoot == null) ? null : detailsRoot.getAsJsonObject("OperatingStatusText");
		JsonPrimitive jsonOperating1 = (jsonOperating0 == null) ? null 		: jsonOperating0.getAsJsonPrimitive("$");
		String operatingStatus 		 = (jsonOperating1 == null) ? "unknown" : jsonOperating1.getAsString();
		
		// Legal form
		JsonObject jsonLegal0 	 = companyRoot.getAsJsonObject("RegisteredDetail");
		JsonObject jsonLegal1 	 = (jsonLegal0 == null) ? null 		: jsonLegal0.getAsJsonObject("LegalFormDetails");
		JsonObject jsonLegal2 	 = (jsonLegal1 == null) ? null 		: jsonLegal1.getAsJsonObject("LegalFormText");
		JsonPrimitive jsonLegal3 = (jsonLegal2 == null) ? null 		: jsonLegal2.getAsJsonPrimitive("$");
		String legalForm	     = (jsonLegal3 == null) ? "unknown" : jsonLegal3.getAsString();
		
		// Employee figures
		JsonObject jsonEmployee0    = companyRoot.getAsJsonObject("EmployeeFigures");
		JsonObject jsonEmployee1    = (jsonEmployee0 == null) ? null 		: jsonEmployee0.getAsJsonObject("IndividualEntityEmployeeDetails");
		JsonPrimitive jsonEmployee2 = (jsonEmployee1 == null) ? null 		: jsonEmployee1.getAsJsonPrimitive("TotalEmployeeQuantity");
		String amountOfEmployees    = (jsonEmployee2 == null) ? "unknown" : jsonEmployee2.getAsString();
		
		// Principal
		JsonObject jsonPrincipal0 	 = companyRoot.getAsJsonObject("PrincipalsAndManagement");
		JsonArray jsonPrincipal1  	 = (jsonPrincipal0 == null) ? null 		: jsonPrincipal0.getAsJsonArray("CurrentPrincipal");
		JsonObject jsonPrincipal2 	 = (jsonPrincipal1 == null) ? null 		: jsonPrincipal1.get(0).getAsJsonObject();
		JsonObject jsonPrincipal3 	 = (jsonPrincipal2 == null) ? null 		: jsonPrincipal2.getAsJsonObject("PrincipalName");
		JsonPrimitive jsonPrincipal4 = (jsonPrincipal3 == null) ? null 	    : jsonPrincipal3.getAsJsonPrimitive("FullName");
		String principal 			 = (jsonPrincipal4 == null) ? "unknown" : jsonPrincipal4.getAsString();

		// Global/Domestic ultimate organisations
		JsonObject linkageRoot 		   = companyRoot.getAsJsonObject("Linkage");
		JsonObject globalLinkageRoot   = (linkageRoot == null) ? null : linkageRoot.getAsJsonObject("GlobalUltimateOrganization");
		JsonObject domesticLinkageRoot = (linkageRoot == null) ? null : linkageRoot.getAsJsonObject("DomesticUltimateOrganization");
		
		// DUNS number
		JsonPrimitive jsonGlobal   = (globalLinkageRoot == null)   ? null 	   : globalLinkageRoot.getAsJsonPrimitive("DUNSNumber");
		String globalDUNS   	   = (jsonGlobal == null)   	   ? "unknown" : jsonGlobal.getAsString();
		JsonPrimitive jsonDomestic = (domesticLinkageRoot == null) ? null 	   : domesticLinkageRoot.getAsJsonPrimitive("DUNSNumber");
		String domesticDUNS 	   = (jsonDomestic == null) 	   ? null 	   : jsonDomestic.getAsString();
		
		// Company name
		JsonArray jsonCompGlobal0  		= (globalLinkageRoot == null)   ? null 	   	: globalLinkageRoot.getAsJsonArray("OrganizationPrimaryName");
		JsonObject jsonCompGlobal1 		= (jsonCompGlobal0 == null) 	? null 		: jsonCompGlobal0.get(0).getAsJsonObject();
		JsonObject jsonCompGlobal2 		= (jsonCompGlobal1 == null) 	? null 		: jsonCompGlobal1.getAsJsonObject("OrganizationName");
		JsonPrimitive jsonCompGlobal3   = (jsonCompGlobal2 == null) 	? null 		: jsonCompGlobal2.getAsJsonPrimitive("$");
		String globalName   		    = (jsonCompGlobal3 == null) 	? "unknown" : jsonCompGlobal3.getAsString();
		JsonArray jsonCompDomestic0     = (domesticLinkageRoot == null) ? null 	    : domesticLinkageRoot.getAsJsonArray("OrganizationPrimaryName");
		JsonObject jsonCompDomestic1    = (jsonCompDomestic0 == null) 	? null 		: jsonCompDomestic0.get(0).getAsJsonObject();
		JsonObject jsonCompDomestic2    = (jsonCompDomestic1 == null) 	? null 		: jsonCompDomestic1.getAsJsonObject("OrganizationName");
		JsonPrimitive jsonCompDomestic3 = (jsonCompDomestic2 == null) 	? null 		: jsonCompDomestic2.getAsJsonPrimitive("$");
		String domesticName 			= (jsonCompDomestic3 == null) 	? "unknown" : jsonCompDomestic3.getAsString();
		
		
		// Address
		JsonArray globalAddress0 	   = (globalLinkageRoot == null)   ? null 	   : globalLinkageRoot.getAsJsonArray("PrimaryAddress");
		JsonObject globalAddressRoot   = (globalAddress0 == null)      ? null 	   : globalAddress0.get(0).getAsJsonObject();
		JsonArray domesticAddress0 	   = (domesticLinkageRoot == null) ? null 	   : domesticLinkageRoot.getAsJsonArray("PrimaryAddress");
		JsonObject domesticAddressRoot = (domesticAddress0 == null)    ? null 	   : domesticAddress0.get(0).getAsJsonObject();
		JsonArray globalStreet0 	   = (globalAddressRoot == null)   ? null 	   : globalAddressRoot.getAsJsonArray("StreetAddressLine");
		JsonObject globalStreet1 	   = (globalStreet0 == null) 	   ? null 	   : globalStreet0.get(0).getAsJsonObject();
		JsonPrimitive globalStreet2    = (globalStreet1 == null) 	   ? null 	   : globalStreet1.getAsJsonPrimitive("LineText");
		String globalAddress   		   = (globalStreet2 == null) 	   ? "unknown" : globalStreet2.getAsString();
		JsonArray domesticStreet0 	   = (domesticAddressRoot == null) ? null 	   : domesticAddressRoot.getAsJsonArray("StreetAddressLine");
		JsonObject domesticStreet1 	   = (domesticStreet0 == null) 	   ? null 	   : domesticStreet0.get(0).getAsJsonObject();
		JsonPrimitive domesticStreet2  = (domesticStreet1 == null) 	   ? null 	   : domesticStreet1.getAsJsonPrimitive("LineText");
		String domesticAddress 		   = (domesticStreet2 == null) 	   ? "unknown" : domesticStreet2.getAsString();
		JsonPrimitive globalTown0 	   = (globalAddressRoot == null)   ? null 	   : globalAddressRoot.getAsJsonPrimitive("PrimaryTownName");
		String globalTown   		   = (globalTown0 == null) 		   ? "unknown" : globalTown0.getAsString();
		JsonPrimitive domesticTown0    = (domesticAddressRoot == null) ? null 	   : domesticAddressRoot.getAsJsonPrimitive("PrimaryTownName");
		String domesticTown 		   = (domesticTown0 == null) 	   ? "unknown" : domesticTown0.getAsString();
		JsonPrimitive globalCountry0   = (globalAddressRoot == null)   ? null 	   : globalAddressRoot.getAsJsonPrimitive("CountryISOAlpha2Code");
		String globalCountryCode   	   = (globalCountry0 == null) 	   ? "unknown" : globalCountry0.getAsString();
		JsonPrimitive domesticCountry0 = (domesticAddressRoot == null) ? null 	   : domesticAddressRoot.getAsJsonPrimitive("CountryISOAlpha2Code");
		String domesticCountryCode	   = (domesticCountry0 == null)    ? "unknown" : domesticCountry0.getAsString();
		JsonPrimitive globalPostal0    = (globalAddressRoot == null)   ? null 	   : globalAddressRoot.getAsJsonPrimitive("PostalCode");
		String globalPostalCode   	   = (globalPostal0 == null) 	   ? "unknown" : globalPostal0.getAsString();
		JsonPrimitive domesticPostal0  = (domesticAddressRoot == null) ? null 	   : domesticAddressRoot.getAsJsonPrimitive("PostalCode");
		String domesticPostalCode 	   = (domesticPostal0 == null) 	   ? "unknown" : domesticPostal0.getAsString();
		
		
		// Global ultimate company
		Company globalUltimate   = new Company(globalName,globalDUNS,globalAddress,globalPostalCode,globalTown,globalCountryCode);
		Company domesticUltimate = new Company(domesticName,domesticDUNS,domesticAddress,domesticPostalCode,domesticTown,domesticCountryCode);
		
		// Return new detailed company through its builder
		return DetailedCompany.Builder.build(basicCompany).setSummary(summary).setSalesRevenue(salesRevenue).setCurrency(currency).setStartYear(startYear).
														   setStandaloneOrganization(isStandalone).setOwnership(ownerShip).setCurrentOwnerStartYear(ownerSince).
														   setOperatingStatus(operatingStatus).setLegalForm(legalForm).setAmountOfEmployees(amountOfEmployees).
														   setPrincipalFullName(principal).setGlobalUltimateOrganization(globalUltimate).setDomesticUltimateOrganization(domesticUltimate).get();
	}
}
