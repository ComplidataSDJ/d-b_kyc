package be.complidata.servlets.actions;

import java.io.IOException;
import java.util.Optional;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import be.complidata.communication.HttpsConnector;

/**
 * Servlet handling the authentication for obtaining a (24hr valid) token from D&B REST service.
 */
@WebServlet("/authenticate")
public class AuthenticationServlet extends HttpServlet {
	private static final long serialVersionUID = -1348373561331170287L;


	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Forward to view
		getServletContext().getRequestDispatcher("/WEB-INF/index.jsp").forward(request, response);
	}
	
	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		HttpSession session = request.getSession();
		
		// Try to obtain token from D&B authorisation service
		Optional<String> token = new HttpsConnector().getAuthenticationToken();
		
		// Failed to authenticate
		if (!token.isPresent()) {
			// Set error message
			request.setAttribute("msgErrorAuthentication", "Failed to authenticate...");
			
			// Forward to view (client refresh will re-send authentication request)
			request.getServletContext().getRequestDispatcher("/WEB-INF/index.jsp").forward(request, response);
			return;
		}
		
		// Valid at this point -> store authentication token in session for ease of use
		session.setAttribute("authToken", token.get());
		
		// PRG-pattern :  redirect to view on success (finishes POST, so refresh will not re-send authentication request)
		response.sendRedirect("/company_lookup");
	}
}
