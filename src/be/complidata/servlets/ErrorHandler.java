package be.complidata.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class ErrorHandler
 */
@WebServlet("/error_handler")
public class ErrorHandler extends HttpServlet {
	private static final long serialVersionUID = 7056119549376319504L;

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processError(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processError(request, response);
    }
    
    private void processError(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("[ERRORHANDLER REACHED]  Report:");
        
        // Request error attributes
        Throwable throwable = (Throwable) request.getAttribute("javax.servlet.error.exception");
        Integer statusCode  = (Integer) request.getAttribute("javax.servlet.error.status_code");
        String servletName  = (String) request.getAttribute("javax.servlet.error.servlet_name");
        String requestUri   = (String) request.getAttribute("javax.servlet.error.request_uri");
        
        // Null checks + string conversion
        String throwableStr   = (throwable == null) ? "Unknown (null)" : throwable.toString();
        String statusCodeStr  = (statusCode == null) ? "Unknown (null)" : statusCode.toString();
        String servletNameStr = (servletName == null) ? "Unknown (null)" : servletName;
        String requestUriStr  = (requestUri == null) ? "Unknown (null)" : requestUri;
        
        
        // TODO If we're going to keep using this servlet, send information to logger
        System.out.println("\t Throwable      :  " + throwableStr);
        System.out.println("\t Status code    :  " + statusCodeStr);
        System.out.println("\t While handling :  " + servletNameStr + " - " + requestUriStr);
        
        if (throwable != null && statusCode != null && servletName != null)
        	request.setAttribute("msgError", "Servlet " + servletName + " has thrown an exception " + throwable.toString() + " status (" + statusCode + ") : " + throwable.getMessage());
        else
        	request.setAttribute("msgError", "Some server-side error (throwable was null)");
        
        getServletContext().getRequestDispatcher("/error.jsp").forward(request, response);
    }
}