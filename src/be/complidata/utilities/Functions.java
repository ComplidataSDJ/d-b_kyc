package be.complidata.utilities;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import be.complidata.models.BeneficialOwner;

public class Functions {

	
	public static List<List<String>> prepareForExport(List<BeneficialOwner> outputList) {
		List<List<String>> result = new ArrayList<>();
		
		// Header
		result.add(Arrays.asList("Company Name", "DUNS Number", "Address", "Postal/ZIP Code", "City/Town", "Country ISO Code", "Relation Type", "Degree of Separation", "Shareholding Percentage"));
		
		// Body
		for (BeneficialOwner beneficialOwner : outputList) {
			// Build beneficial owner
			List<String> beneficialOwnerAsList = new ArrayList<>();
			beneficialOwnerAsList.add(beneficialOwner.getName());
			beneficialOwnerAsList.add(beneficialOwner.getNumberDUNS());
			beneficialOwnerAsList.add(beneficialOwner.getAddress());
			beneficialOwnerAsList.add(beneficialOwner.getPostalCode());
			beneficialOwnerAsList.add(beneficialOwner.getCityTown());
			beneficialOwnerAsList.add(beneficialOwner.getCountryISO());
			beneficialOwnerAsList.add(beneficialOwner.getRelationType());
			beneficialOwnerAsList.add(String.valueOf(beneficialOwner.getDegreeOfSeparation()));
			beneficialOwnerAsList.add(beneficialOwner.getShareHoldingPercentage());
			
			// Add to result builder
			result.add(beneficialOwnerAsList);
		}
		
		return result;
	}
}
