$(".form-100-btn.data.risk").click(function () {
	var companyName = $(".input-company-name").val();
	console.log(companyName);
	if(companyName == "HSBC BANK (SINGAPORE) LIMITED")
	{
		WshShell = new ActiveXObject("WScript.Shell");
		WshShell.Run("../WebContent/assets/excel/RiskSCoreCardEnterpriseHSBC.xls", 1, false);
	} 
	else if(companyName == "LEE HONG LEEN HOLDINGS PTE.LT D.")
	{
		WshShell = new ActiveXObject("WScript.Shell");
		WshShell.Run("../WebContent/assets/excel/RiskSCoreCardEnterpriseLeeHohnLeen.xls", 1, false);
	}
	else if(companyName == "SPC MANUFACTURING")
	{
		WshShell = new ActiveXObject("WScript.Shell");
		WshShell.Run("../WebContent/assets/excel/RiskSCoreCardEnterpriseSPCManufacturing.xls", 1, false);
	}
});

$(".form-100-btn.data.shareholders").click(function () {
	var companyName = $(".input-company-name").val();
	console.log(companyName);
	if(companyName == "HSBC BANK (SINGAPORE) LIMITED")
	{
		WshShell = new ActiveXObject("WScript.Shell");
		WshShell.Run("../WebContent/assets/excel/HSBC Shareholders.xls", 1, false);
	} 
	else if(companyName == "LEE HONG LEEN HOLDINGS PTE.LT D.")
	{
		WshShell = new ActiveXObject("WScript.Shell");
		WshShell.Run("../WebContent/assets/excel/LeeHonhLeen Shareholders.xls", 1, false);
	}
	else if(companyName == "SPC MANUFACTURING")
	{
		WshShell = new ActiveXObject("WScript.Shell");
		WshShell.Run("../WebContent/assets/excel/SPC MANUFACTURING Shareholders.xls", 1, false);
	}
});


(function ($) {
    "use strict";


    /*==================================================================
    [ Focus input ]*/
    $('.input100').each(function(){
        $(this).on('blur', function(){
            if($(this).val().trim() != "") {
                $(this).addClass('has-val');
            }
            else {
                $(this).removeClass('has-val');
            }
        })    
    })
  
  
    /*==================================================================
    [ Validate ]*/
    var input = $('.validate-input .input100');

    $('.validate-form').on('submit',function(){
        var check = true;

        for(var i=0; i<input.length; i++) {
            if(validate(input[i]) == false){
                showValidate(input[i]);
                check=false;
            }
        }

        return check;
    });


    $('.validate-form .input100').each(function(){
        $(this).focus(function(){
           hideValidate(this);
        });
    });

    function validate (input) {
        if($(input).attr('type') == 'email' || $(input).attr('name') == 'email') {
            if($(input).val().trim().match(/^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{1,5}|[0-9]{1,3})(\]?)$/) == null) {
                return false;
            }
        }
        else {
            if($(input).val().trim() == ''){
                return false;
            }
        }
    }

    function showValidate(input) {
        var thisAlert = $(input).parent();

        $(thisAlert).addClass('alert-validate');
    }

    function hideValidate(input) {
        var thisAlert = $(input).parent();

        $(thisAlert).removeClass('alert-validate');
    }
    
    /*==================================================================
    [ Show pass ]*/
    var showPass = 0;
    $('.btn-show-pass').on('click', function(){
        if(showPass == 0) {
            $(this).next('input').attr('type','text');
            $(this).addClass('active');
            showPass = 1;
        }
        else {
            $(this).next('input').attr('type','password');
            $(this).removeClass('active');
            showPass = 0;
        }
        
    });


})(jQuery);