<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">

<jsp:include page="head.jsp">
	<jsp:param name="title" value="Company Details"/>
</jsp:include>

<body>

	<div class="limiter">
		<div class="container-100" style="background-image: url('assets/images/bg-01.jpg');">
		
			<%-- COMPANY LOOKUP ERROR MESSAGE --%>
			<c:if test="${not empty requestScope.msgErrorCompanyLookup})">
				<span class="msg-error-company-lookup">${requestScope.msgErrorCompanyLookup}</span>
			</c:if>

			<div class="wrap-100">
				<span class="form-100-title p-b-34 p-t-27" style="color: black;">
					Detailed Company Overview:
				</span>
				<table style="margin: 0 auto;">
					<tr class="tr-details">
						<td style="font-weight:bold" class="td-details">Company name :</td>
						<td class="td-details">${sessionScope.detailedCompany.name}</td>
					</tr>
					<tr class="tr-details">
						<td style="font-weight:bold" class="td-details">DUNS number :</td>
						<td class="td-details">${sessionScope.detailedCompany.numberDUNS}</td>
					</tr>
					<tr class="tr-details">
						<td style="font-weight:bold" class="td-details">Address :</td>
						<td class="td-details">${sessionScope.detailedCompany.address}</td>
					</tr>
					<tr class="tr-details">
						<td style="font-weight:bold" class="td-details">Postal code :</td>
						<td class="td-details">${sessionScope.detailedCompany.postalCode}</td>
					</tr>
					<tr class="tr-details">
						<td style="font-weight:bold" class="td-details">City / Town :</td>
						<td class="td-details">${sessionScope.detailedCompany.cityTown}</td>
					</tr>
					<tr class="tr-details">
						<td style="font-weight:bold" class="td-details">Country ISO code :</td>
						<td class="td-details">${sessionScope.detailedCompany.countryISO}</td>
					</tr>
					<tr class="tr-details">
						<td style="font-weight:bold" class="td-details">Summary :</td>
						<td class="td-details">${sessionScope.detailedCompany.summary}</td>
					</tr>
					<tr class="tr-details">
						<td style="font-weight:bold" class="td-details">Sales revenue :</td>
						<td class="td-details">${sessionScope.detailedCompany.salesRevenue} ${sessionScope.detailedCompany.currency} </td>
					</tr>
					<tr class="tr-details">
						<td style="font-weight:bold" class="td-details">Standalone organization :</td>
						<td class="td-details">${sessionScope.detailedCompany.standaloneOrganization}</td>
					</tr>
					<tr class="tr-details">
						<td style="font-weight:bold" class="td-details">Operating status :</td>
						<td class="td-details">${sessionScope.detailedCompany.operatingStatus}</td>
					</tr>
					<tr class="tr-details">
						<td style="font-weight:bold" class="td-details">Founded in :</td>
						<td class="td-details">${sessionScope.detailedCompany.startYear}</td>
					</tr>
					<tr class="tr-details">
						<td style="font-weight:bold" class="td-details">Ownership :</td>
						<td class="td-details">${sessionScope.detailedCompany.ownership}</td>
					</tr>
					<tr class="tr-details">
						<td style="font-weight:bold" class="td-details">Owner since :</td>
						<td class="td-details">${sessionScope.detailedCompany.currentOwnerStartYear}</td>
					</tr>
					<tr class="tr-details">
						<td style="font-weight:bold" class="td-details">Legal form :</td>
						<td class="td-details">${sessionScope.detailedCompany.legalForm}</td>
					</tr>
					<tr class="tr-details">
						<td style="font-weight:bold" class="td-details">Amount of employees :</td>
						<td class="td-details">${sessionScope.detailedCompany.amountOfEmployees}</td>
					</tr>
					<tr class="tr-details">
						<td style="font-weight:bold" class="td-details">Principal full name :</td>
						<td class="td-details">${sessionScope.detailedCompany.principalFullName}</td>
					</tr>
					<tr class="tr-details">
						<td style="font-weight:bold" class="td-details">Global ultimate organization :</td>
						<td class="td-details">
							<c:set var="globalOrg" value="${sessionScope.detailedCompany.globalUltimateOrganization}" />
							<i>Name :</i> ${globalOrg.name}<br>
							<i>DUNS number :</i> ${globalOrg.numberDUNS}<br>
							<i>Address :</i> ${globalOrg.address}<br>
							<i>Postal code :</i> ${globalOrg.postalCode}<br>
							<i>City/Town :</i> ${globalOrg.cityTown}<br>
							<i>Country ISO code :</i> ${globalOrg.countryISO}
						</td>
					</tr>
					<tr class="tr-details">
						<td style="font-weight:bold; padding: 10px 20px;">Domestic ultimate organization :</td>
						<td class="td-details">
							<c:set var="domesticOrg" value="${sessionScope.detailedCompany.domesticUltimateOrganization}" />
							<i>Name :</i> ${domesticOrg.name}<br>
							<i>DUNS number :</i> ${domesticOrg.numberDUNS}<br>
							<i>Address :</i> ${domesticOrg.address}<br>
							<i>Postal code :</i> ${domesticOrg.postalCode}<br>
							<i>City/Town :</i> ${domesticOrg.cityTown}<br>
							<i>Country ISO code :</i> ${domesticOrg.countryISO}
						</td>
					</tr>
				</table>
				
				<br/>
				<br/>
				
				<form action="/beneficial_owners_lookup" method="GET">
					<div class="container-100-form-btn">
						<button type="submit" value="Show beneficial owners" class="form-100-btn">Show beneficial owners</button>
					</div>
				</form>
			</div>
		</div>
	</div>
				
</body>
	
</html>