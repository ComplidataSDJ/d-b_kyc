<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">


<head><link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/../assets/css/main.css"></head>

<body>

	<%-- COMPANY LOOKUP ERROR MESSAGE --%>
	<c:if test="${not empty requestScope.msgErrorCompanyLookup})">
		<span class="msg-error-company-lookup">${requestScope.msgErrorCompanyLookup}</span>
	</c:if>

	<%-- AUTHENTICATION FORM --%>
	<div class="limiter">
		<div class="container-100" style="background-image: url('assets/images/bg-01.jpg');">
			<div class="wrap-100">
				<form method="POST" action="/authenticate" class="form-100 validate-form">
					<span class="form-100-logo">
						<img class="form-100-logo-image" src="assets/images/kyckr-logo.png">
					</span>

					<br/>
					<br/>
						
					<span class="form-100-title p-b-34 p-t-27">
						Company Lookup
					</span>

					<br/>
					<br/>
					
					<div class="container-100-form-btn">
						<button type="submit" class="form-100-btn">Search Company</button>
					</div>
					
					<br/>

				</form>
			</div>
		</div>
	</div>
	

	<div id="dropDownSelect1"></div>
</body>

<%--=============================================================================================== --%>
	<script type="text/javascript" src="${pageContext.request.contextPath}/assets/vendor/jquery/jquery-3.2.1.min.js"></script>
<%--===============================================================================================--%>
	<script type="text/javascript" src="${pageContext.request.contextPath}/assets/vendor/animsition/js/animsition.min.js"></script>
<%--===============================================================================================--%>
	<script type="text/javascript" src="${pageContext.request.contextPath}/assets/vendor/bootstrap/js/popper.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/assets/vendor/bootstrap/js/bootstrap.min.js"></script>
<%--===============================================================================================--%>
	<script type="text/javascript" src="${pageContext.request.contextPath}/assets/vendor/select2/select2.min.js"></script>
<%--===============================================================================================--%>
	<script type="text/javascript" src="${pageContext.request.contextPath}/assets/vendor/daterangepicker/moment.min.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/assets/vendor/daterangepicker/daterangepicker.js"></script>
<%--===============================================================================================--%>
	<script type="text/javascript" src="${pageContext.request.contextPath}/assets/vendor/countdowntime/countdowntime.js"></script>
<%--===============================================================================================--%>
	<script type="text/javascript" src="${pageContext.request.contextPath}/assets/js/main.js"></script>

</html>