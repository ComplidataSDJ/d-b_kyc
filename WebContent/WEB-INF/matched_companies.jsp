<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">

<jsp:include page="head.jsp">
	<jsp:param name="title" value="Matched Companies"/>
</jsp:include>
<head>
		<%-- CSS to fake HTML table and allow <form> elements to function as rows --%>
		<style>
			div.table 		 { display	   : table; }
			div.tr, form.tr  { display	   : table-row; }
			span.td, span.th { display	   : table-cell; }
			span.th 		 { font-weight : bold; }
		</style>
</head>

<body>

	<div class="limiter">
		<div class="container-100" style="background-image: url('assets/images/bg-01.jpg');">
		
			<%-- COMPANY LOOKUP ERROR MESSAGE --%>
			<c:if test="${not empty requestScope.msgErrorCompanyLookup})">
				<span class="msg-error-company-lookup">${requestScope.msgErrorCompanyLookup}</span>
			</c:if>

			<div class="wrap-100" style="width: 1600px">
				<span class="form-100-title p-b-34 p-t-27" style="color: black;">
					Matched Companies:
				</span>
				<div class="table" style="width: 90%; margin: 0 auto;">
				    <div class="tr">
			      	  <span class="th th-matched-companies">Company name</span>
			      	  <span class="th th-matched-companies">DUNS number</span>
			      	  <span class="th th-matched-companies">Address</span>
			      	  <span class="th th-matched-companies">Postal code</span>
			      	  <span class="th th-matched-companies">City/Town</span>
			      	  <span class="th th-matched-companies">Country ISO code</span>
			       	  <span class="th th-matched-companies"></span>
			    	</div>
					<c:forEach items="${sessionScope.matchedCompanies}" var="company">
						<form class="tr tbody-matched-companies" method="GET" action="/company_details_lookup">
							<span class="td td-matched-companies">${company.name}</span>
							<span class="td td-matched-companies"><input type="hidden" name="numberDUNS" value="${company.numberDUNS}" />${company.numberDUNS}</span>
							<span class="td td-matched-companies">${company.address}</span>
							<span class="td td-matched-companies">${company.postalCode}</span>
							<span class="td td-matched-companies">${company.cityTown}</span>
							<span class="td td-matched-companies">${company.countryISO}</span>
							<span class="td td-matched-companies"><input type="submit" value="Details" class="company-details-btn"/></span>
						</form>
					</c:forEach>
				</div>
			</div>

		</div>
	</div>
				
</body>
	
</html>