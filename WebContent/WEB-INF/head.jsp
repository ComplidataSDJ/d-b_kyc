<head>
	<title>${param.title}</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<%-- INCLUDING ALL VENDOR FILES --%>
<%--===============================================================================================--%>	
	<link rel="icon" type="image/png" href="${pageContext.request.contextPath}/assets/images/icons/favicon.ico"/>
<%--===============================================================================================--%>
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/assets/vendor/bootstrap/css/bootstrap.min.css">
<%--===============================================================================================--%>
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/assets/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<%--===============================================================================================--%>
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/assets/fonts/iconic/css/material-design-iconic-font.min.css">
<%--===============================================================================================--%>
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/assets/vendor/animate/animate.css">
<%--===============================================================================================--%>	
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/assets/vendor/css-hamburgers/hamburgers.min.css">
<%--===============================================================================================--%>
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/assets/vendor/animsition/css/animsition.min.css">
<%--===============================================================================================--%>
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/assets/vendor/select2/select2.min.css">
<%--===============================================================================================--%>	
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/assets/vendor/daterangepicker/daterangepicker.css">

<%-- INCLUDING OWN CSS FILES --%>
<%--===============================================================================================--%>
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/assets/css/util.css">
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/assets/css/main.css">
<%--===============================================================================================--%>
</head>