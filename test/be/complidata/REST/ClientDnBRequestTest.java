package be.complidata.REST;

import org.junit.Before;
import org.junit.Test;


public class ClientDnBRequestTest {
	
	ClientDnB client;
	@Before
	public void setUp() {
		client = new ClientDnB();
		
		client.authenticate();
	}	
	
	
	@Test
	public void testAuth() {
		// Test authentication + token
		client.authenticate();
	}
	
	
	@Test
	public void testMatchedCompaniesLookup() {
		// Try to lookup a company by subject name
		client.lookupCompanyMatchesBySubjectName("mc2bis", "BE");
	}
	
	
	@Test
	public void testMatchedCompanyLookupRegCode() {
		// Try to lookup a company by Company Registration Code
		client.lookupCompanyMatchesByRegCode("0460606478", "BE");
	}
	
	
	@Test
	public void testDetailedCompanyLookup() {
		// Try to lookup a company by DUNS Number
		// DUNS number of MC2BIS BE
		client.lookupDetailedCompanyByDUNS("763898108");
	}
	
	
	@Test
	public void testUBOLookup() {
		// Try to lookup a company by DUNS Number
		// DUNS number of Google BE
		client.lookupUBOByDUNS("763817751");
	}
}
